// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

// Настройки
const setup = { port: 8089 };
// Подключаем express
const express = require('express');
// Подключаем mongoDB native driver
const mongodb = require('mongodb');
const mongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
// Парсеры
const cookieParser = require('cookie-parser');
const jsonParser = express.json();
// const urlencodedParser = express.urlencoded({extended: false});
// Библиотека, распознающая IP клиента даже за прокси
const getIP = require('ipware')().get_ip;

// Создаем приложение
const app = express();
// Подключаем WebSockets
const expressWs = require('express-ws')(app);

// Глобальные переменные
let db;
let sessions;
let failBan;
let directories;
let documents;
const settings = {};
let server;

app.use(cookieParser('ArknightsMemesGalore/expectUnexpectable'));
app.set('view engine', 'handlebars');
app.enable('view cache');

// Коннектимся к базе данных
mongoClient.connect('mongodb://easterntime:MyWholeBodyHurtsF^CK@localhost:27017/easternTimeToolset', (err, database) => {
  if (err) throw err;
  db = database.db('easternTimeToolset');
  sessions = db.collection('cookies');
  failBan = db.collection('failBan');
  directories = db.collection('directories');
  documents = db.collection('documents');
  settings.tagsPaired = db.collection('tagsPaired');
  settings.tagsSingle = db.collection('tagsSingle');
  settings.colors = db.collection('colors');

  server = app.listen(setup.port, () => {
    console.log('Server on port %s started', setup.port);
  });
});

// Прямое подключение к серверу
const aWss = expressWs.getWss('/tools/ws');

app.ws('/tools/ws', function(ws, req) {
  // Отправить количество пользователей онлайн всем юзерам
  const sendCount = function() {
    const msg = JSON.stringify({
      request: 'users',
      count: aWss.clients.size,
    });
    aWss.clients.forEach(client => client.send(msg));
  };
  // Возвращает строку для отправки, сообщение об ошибке
  const generateError = function(msg) {
    const err = JSON.stringify({
      request: 'error',
      err: msg,
    });
    return err;
  };

  ws.on('open', () => {
    sendCount();
  });

  ws.on('close', () => {
    sendCount();
  });

  ws.on('message', function(msg) {
    try {
      const msgObj = JSON.parse(msg);
      // Апдейт: клиент желает внести изменения в документ
      if (msgObj.request === 'update') {
        // Формат запроса:
        // session - печенька пользователя
        // uniqueID - какая именно вкладка пользователя отправила сообщение
        // documentID - уникальный id документа
        // [Поля документа, которые нужно изменить]

        if (!msgObj.documentID) { ws.send(generateError('Отсутствует необходимое поле: documentID.')); };

        let o_id;
        try {
          o_id = new mongodb.ObjectID(msgObj.documentID);
        } catch (e) {
          ws.send(generateError('Некорректный documentID.'));
        }

        const allowedFields = ['name', 'header', 'preheader', 'bannerLink', 'buttonColor',
          'linkDarker', 'fontColor', 'gif1Link', 'gif2Link', 'mainLink', 'mainText', 'importText', 'notesText'];

        const args = {};

        for (const [key, value] of Object.entries(msgObj)) {
          if (allowedFields.includes(key)) {
            args[key] = value;
          }
        }

        args.updated = new Date();

        documents.updateOne({ _id: o_id }, { $set: args })
          .then(res => {
            // Рассылаем всем клиентам апдейт
            const responseText = JSON.stringify({
              request: 'update',
              uniqueID: msgObj.uniqueID,
              documentID: msgObj.documentID,
              ...args,
            });
            aWss.clients.forEach(client => client.send(responseText));
          })
          .catch(e => {
            ws.send(generateError(e));
          });
        // Хэндшейк: клиент желает получить список людей на сервере
      } else if (msgObj.request === 'handshake') {
        // ws.send(JSON.stringify({
        // request: 'users',
        // count: aWss.clients.size
        // }));
        sendCount();
      }
    } catch (e) {
      console.log(e);
    }
  });
});

// const encodeTimeStamp = function() {
//   return Math.floor(Date.now() / 1000);
// };

// Проверяет, пуст ли переданный объект
const isEmpty = function(obj) {
  if (!obj) { return true; }
  // eslint-disable-next-line no-unreachable-loop
  for (const i in obj) { return false; }
  return true;
};

// Если переменная пустая, ставит значение по умолчанию
const def = function(a, defaultValue) {
  return a == null ? defaultValue : a;
};

// Конвертирует строку/число в булево значение
const castToBool = function(str) {
  return (str === true || str === 'true' || str > 0);
};

// Возвращает true в случае успеха или reject'ит с кодом ошибки
const passCheck = (pwd, ip) => new Promise((resolve, reject) => {
  const rightPwd = '123'; // Это нужно переделать в секрет

  return failBan.findOne({ ip }).then(res => {
    console.log(res);
    let failedCount = 0;
    if (res) {
      failedCount = res.failedCount;
      if (failedCount > 4) {
        return reject('Слишком много неверных вводов пароля. Подождите несколько минут.');
      }
    }
    if (pwd === rightPwd) {
      return resolve(true);
    } else {
      failBan.updateOne({ ip }, { $inc: { failedCount: 1 }, $set: { updated: new Date() } }, { upsert: true });
      if (failedCount > 3) {
        return reject('Слишком много неверных вводов пароля. Подождите несколько минут.');
      } else {
        return reject('Неверный пароль. Попробуйте ещё раз.');
      }
    }
  });
});

// Наша основная функция. Должна вернуть _id
// Логика функции такая.
// Если есть кука - ищем объект. Если он найден - апдейтим его.
// Во всех остальных случаях мы должны инзертнуть новый объект.
const sessionNew = function(myCookie, myLogin, ip) {
  // Вставка новой сессии
  console.log(`New session: ${myLogin} - ${ip}`);
  const sessionInsert = () => new Promise((resolve, reject) => {
    return sessions.insertOne({
      ip,
      login: myLogin,
      created: new Date(),
      updated: new Date(), // new Date(Date.now() + 36000000)
    }).then(res => {
      return resolve(res.insertedId.toString());
    }).catch(e => { console.log(`Error while trying to insert: ${e}`); });
  });

  // Основная часть
  // Если у нас есть кука
  if (myCookie.token) {
    let o_id;
    try {
      o_id = new mongodb.ObjectID(myCookie.token);
    } catch (e) {
      console.log('Cookie is corrupted. ' + e);
      return sessionInsert();
    }
    // Если в куке валидный айди, то апдейтим/инзертим
    return sessions.updateOne({ _id: o_id }, {
      $set: {
        ip,
        login: myLogin,
        created: new Date(),
        updated: new Date(),
      },
    }, { upsert: true })
      .then(res => {
        // Отдаём домой апдейтнутый _id
        return myCookie.token;
      })
      .catch((e) => { console.log(`Error while trying to update session id ${o_id}: ${e}`); });
  } else {
    // Если нет печеньки, вставляем новую запись
    return sessionInsert();
  }
};

// Проверяем сессию на существование и годность
const sessionVerify = (myCookie, ip) => new Promise((resolve, reject) => {
  // Проверка печеньки
  let o_id;
  try {
    o_id = new mongodb.ObjectID(myCookie.token);
  } catch (e) {
    return reject('Печенька отсутствует или не валидна.');
  }
  return sessions.findOne({ _id: o_id, ip }).then(res => {
    if (res) {
      sessions.updateOne({ _id: o_id, ip }, { $set: { updated: new Date() } });
      return resolve(true);
    } else {
      return reject('Сессия не существует.');
    }
  });
});

// Маршрутизируем GET-запросы
app.get('/tools/login', (req, response) => {
  response.render('login.hbs', {
    message: 'Пожалуйста, войдите в систему.',
  });
});

app.post('/tools/login', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }
  if (!request.body.pass) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать поле pass.');
  }

  passCheck(request.body.pass, getIP(request).clientIp)
    .then(() => sessionNew(request.cookies, request.body.log, getIP(request).clientIp)
      .then(token => {
        response.send({ code: 'OK', token });
      }))
    .catch(e => {
      response.status(500)
        .type('json')
        .send(JSON.stringify(e));
    });
});

// app.ws('/', (ws, req) => {
// При подключении мы должны проверить id сессии
// {session: ObjectID, document: ObjectID}

// Проверяем id документа

// Если id не нулевой

// Проверяем есть ли такой id в БД

// Отдаём список сессий в этом документе
// });

// wss.on('connection', function connection(ws) {
// ws.on('open', function open() {
//   console.log('Opened connection.');
//   ws.send('something');
// });

// ws.on('message', function message(data) {
// console.log('received: %s', data);
// ws.send(data)
// });
// });

// Все запросы ниже потребуют аутентификации

app.all('*', (request, response, next) => {
  sessionVerify(request.cookies, getIP(request).clientIp)
    .then(() => {
      next();
    })
    .catch(e => {
      response.type('html');
      response.status(401).send('<!DOCTYPE html><html><head><meta http-equiv="refresh" content="0; url=/tools/login?redirect=' + encodeURIComponent(request.originalUrl) + '"></head></html>');
      // response.redirect('/tools/login?redirect='+encodeURIComponent(request.originalUrl));
    });
});

// Запросы на странички

app.get('/tools', (request, response) => {
  // sessionVerify(request.cookies, getIP(request).clientIp)
  // .then(() => {
  response.render('editor.hbs', { test: false });
  // })
  // .catch(e => {
  // console.log(e);
  // response.redirect('/tools/login?redirect='+encodeURIComponent(request.originalUrl));
  // })
});

app.get('/tools/test', (request, response) => {
  response.render('editorTest.hbs', { test: true });
});

app.get('/tools/old', (request, response) => {
  response.render('editorOld.hbs');
});

app.get('/tools/settings', (request, response) => {
  // sessionVerify(request.cookies, getIP(request).clientIp)
  // .then(() => {
  response.render('settings.hbs');
  // })
  // .catch(e => {
  // console.log(e);
  // response.redirect('/tools/login?redirect='+encodeURIComponent(request.originalUrl));
  // })
});

app.get('/tools/list', (request, response) => {
  // sessionVerify(request.cookies, getIP(request).clientIp)
  // .then(() => {
  response.render('list.hbs');
  // })
  // .catch(e => {
  // console.log(e);
  // response.redirect(401, '/tools/login?redirect='+encodeURIComponent(request.originalUrl));
  // })
});

// API

app.get('/tools/list/all.json', (request, response) => {
  // Возможные квери:
  // directory: id папки, в которой лежат файлы, или "/" для корня
  // search: строка поиска по названию и заголовку
  // page: номер страницы pagination
  // size: номер выборки на каждой странице
  // sortby: поле сортировки, по-умолчанию - дата
  // sortdir: 1 - по возрастанию, 2 - по убыванию
  const args = {};
  args.req = {};
  args.options = {};
  // Папка, в которой мы находимся
  if ((!request.query.directory) || (decodeURI(request.query.directory) === '/')) {
    args.req.parent = '/';
  } else {
    // try {
    // args.req.parent = new mongodb.ObjectID(decodeURI(request.query.directory));
    // } catch {
    // return response.sendStatus(400);
    // }
    args.req.parent = decodeURI(request.query.directory);
  }
  // Поиск
  // Идёт по полям name и header
  if (request.query.search) {
    args.req.$or = [
      { name: new RegExp(decodeURI(request.query.search), 'i') },
      { header: new RegExp(decodeURI(request.query.search), 'i') },
    ];
  }
  // Pagination
  if (request.query.size && parseInt(request.query.size)) {
    args.options.limit = parseInt(request.query.size);
    if (request.query.page && parseInt(request.query.page)) {
      args.options.skip = args.options.limit * parseInt(request.query.page);
    }
  }
  // Сортировка
  if (request.query.sortby) {
    if (['_id', 'name', 'title', 'created', 'updated'].indexOf(decodeURI(request.query.sortby))) {
      let sortdir;
      if (request.query.sortdir && ['asc', 'desc'].indexOf(decodeURI(request.query.sortdir))) {
        sortdir = decodeURI(request.query.sortdir);
      } else {
        sortdir = 'asc';
      }
      args.options.sort = [[decodeURI(request.query.sortby), sortdir], ['created', 'asc']];
    } else {
      args.options.sort = [['created', 'asc']];
    }
  }

  const result = [];

  // В начале списка всегда идут папки, запрашиваем их
  directories.find(args.req, args.options).toArray().then(res => {
    res.forEach(el => {
      const temp = {
        id: el._id.toString(),
        type: 'directory',
        name: el.name,
        parent: el.parent,
        created: el.created.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
        updated: el.updated.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
      };
      result.push(temp);
    });
    // Следом вставляем документы
    documents.find(args.req, args.options).toArray().then(res => {
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'document',
          name: el.name,
          header: def(el.header, ''),
          parent: el.parent,
          created: el.created.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
          updated: el.updated.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
        };
        result.push(temp);
      });
      // Отправляем результат
      response.type('json');
      return response.send(JSON.stringify(result));
    });
  });
});

app.get('/tools/list/directory.json', (request, response) => {
  if (request.query.id) {
    // Проверим ID
    let o_id;
    try {
      o_id = new mongodb.ObjectID(decodeURI(request.query.id));
    } catch (e) {
      return response.status(400)
        .type('text')
        .send('Некорректный id.');
    }
    // Если нам передали ID, возвращаем только папку с этим ID
    directories.findOne({ _id: o_id }).then(res => {
      if (!res) {
        return response.status(404)
          .type('text')
          .send('Документ с id ' + request.query.id + ' не найден.');
      };
      const temp = {
        id: res._id.toString(),
        type: 'directory',
        name: res.name,
        parent: res.parent,
        created: res.created.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
        updated: res.updated.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
      };
      response.type('json');
      return response.send(JSON.stringify(temp));
    });
  } else {
    // Если нам не передели ID, возвращаем все папки
    // TODO: реализовать моар опций
    directories.find({}).toArray().then(res => {
      const result = [];
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'directory',
          name: el.name,
          parent: el.parent,
          created: el.created.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
          updated: el.updated.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
        };
        result.push(temp);
      });
      response.type('json');
      return response.send(JSON.stringify(result));
    });
  }
});

app.get('/tools/list/directorylist.json', (request, response) => {
  // Список всех наших папочек
  let nodes = [];

  // Рекурсивная функция
  // Принимает ноду
  // Ищет всех её детей и добавляет в ноду
  // Возвращает обновлённую ноду
  const getChildren = function(node) {
    const myID = typeof node._id === 'string' ? node._id : node._id.toString();
    // Иерархия задана полем parent, ищем всех детей с нашим parent
    // let result = nodes.filter(thisNode => thisNode.parent === myID);
    const result = [];
    const reducedNodes = [];
    nodes.forEach(el => {
      if (el.parent === myID) {
        result.push(el);
      } else {
        reducedNodes.push(el);
      }
    });
    nodes = reducedNodes;

    // Вызываем эту же функцию для всех детей
    result.forEach(el => {
      el = getChildren(el);
    });

    // Отдаём изменённую ноду
    node.children = result;
    return node;
  };

  if (request.query.id) {
    // Проверим ID
    try {
      const o_id = new mongodb.ObjectID(decodeURI(request.query.id));
      if (!o_id) {
        return response.status(400)
          .type('text')
          .send('Некорректный id.');
      }
    } catch (e) {
      return response.status(400)
        .type('text')
        .send('Некорректный id.');
    }
    // Если нам передали ID, возвращаем путь до папки с этим ID
    // directories.findOne({'_id':o_id}).then(res => {
    // if (!res) {return response.sendStatus(404)};
    // let temp = {
    // id: res._id.toString(),
    // type: 'directory',
    // name: res.name,
    // parent: res.parent,
    // created: res.created.toLocaleString('ru-RU',{timeZone: 'Europe/Moscow'}),
    // updated: res.updated.toLocaleString('ru-RU',{timeZone: 'Europe/Moscow'})
    // }
    // response.type('json');
    // return response.send(JSON.stringify(temp));
    // })
  } else {
    // Если нам не передели ID, возвращаем дерево из всех папок
    // Ох, пора страдать
    directories.find({}).toArray().then(res => {
      nodes = res;
      const result = getChildren({ _id: '/', name: 'root' });

      response.type('json');
      return response.send(JSON.stringify(result));
    });
  }
});

app.post('/tools/list/directory.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  };
  // Создание новой папочки или переименование старой
  // Возможные поля:
  // id - айди папки
  // parent - где создавать новую папку
  // move - новый родитель
  // name - новое имя папки

  if (request.body.id) {
    let o_id;
    try {
      o_id = new mongodb.ObjectID(request.body.id);
    } catch (e) {
      return response.status(400)
        .type('text')
        .send('Некорректный id.');
    }
    const args = {};
    // Если есть аргумент move, двигаем
    // TODO: проверять, что есть такая папка
    if (request.body.move) {
      if (request.body.move === '/') {
        args.parent = '/';
      } else {
        try {
          new mongodb.ObjectID(request.body.move);
        } catch (e) {
          return response.status(400)
            .type('text')
            .send('Некорректный id папки назначения.');
        }
        args.parent = request.body.move;
      }
    }
    // Если есть аргумент name, переименовываем
    if (request.body.name) {
      args.name = request.body.name;
    }
    // Если аргументов нет, то что мы тут делаем?
    if (isEmpty(args)) {
      return response.status(400)
        .type('text')
        .send('Недостаточно аргументов.');
    };

    // Если всё ок, апдейтим
    args.updated = new Date();
    directories.updateOne({ _id: o_id }, { $set: args }).then(res => {
      return response.send(JSON.stringify(res));
    });
  } else {
    // Если нам не дали ID, это создание новой папки
    if (!request.body.name) {
      return response.status(400)
        .type('text')
        .send('Отсутствует необходимое поле name.');
    };
    const sParent = request.body.parent ? request.body.parent : '/';
    directories.insertOne({
      name: request.body.name,
      parent: sParent,
      created: new Date(),
      updated: new Date(),
    }).then(res => {
      return response.send(JSON.stringify(res));
    }).catch(e => { return response.send(JSON.stringify({ code: e })); });
  }
});

app.delete('/tools/list/directory.json', jsonParser, (request, response) => {
  // Удаляет папку под означенным ID
  // допустимое условие: {force: true} заставит удалить папку даже с содержимым
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  };
  if (!request.body.id) {
    return response.status(400)
      .type('text')
      .send('Отсутствует необходимое поле id.');
  };

  let o_id;
  try {
    o_id = new mongodb.ObjectID(request.body.id);
  } catch (e) {
    return response.status(400)
      .type('text')
      .send('Некорректный id.');
  }
  // TODO: Раз уж мы вообще позволяем удалять непустые папки,
  // надо как следует убирать за собой. Нужна рекурсивная функция
  // До тех пор запрещу удалять непустые папки
  directories.findOne({ parent: request.body.id }).then(res1 => {
    if (res1) {
      return response.status(400).send(JSON.stringify({ code: 'not-empty', message: 'Папка не пуста.' }));
    }
    documents.findOne({ parent: request.body.id }).then(res2 => {
      if (res2) {
        return response.status(400).send(JSON.stringify({ code: 'not-empty', message: 'Папка не пуста.' }));
      }
      directories.deleteOne({ _id: o_id }).then(res3 => {
        return response.send(JSON.stringify(res3));
      });
    });
  }).catch(e => {
    return response.status(500)
      .type('json')
      .send(JSON.stringify(e));
  });

  // directories.deleteOne({_id: o_id}).then(() => {
  // directories.deleteMany({'parent': request.body.id})
  // documents.deleteMany({'parent': request.body.id}).then(res => {
  // return response.send(JSON.stringify(res))
  // })
  // }).catch(e => {
  // return response.send(JSON.stringify({'code':e}))
  // });
});

app.get('/tools/list/document.json', (request, response) => {
  // return response.status(400).send('Тестовая ошибка всем тестовым ошибкам!')
  if (request.query.id) {
    // Проверим ID
    let o_id;
    try {
      o_id = new mongodb.ObjectID(decodeURI(request.query.id));
    } catch (e) {
      return response.status(400)
        .type('text')
        .send('Некорректный id.');
    }
    // Если нам передали ID, возвращаем только папку с этим ID
    documents.findOne({ _id: o_id }).then(res => {
      if (!res) {
        return response.status(404)
          .type('text')
          .send('Документ с id ' + request.query.id + ' не найден.');
      }
      const temp = {
        id: res._id.toString(),
        type: 'directory',
        name: def(res.name, ''),
        header: def(res.header, ''),
        preheader: def(res.preheader, ''),
        bannerLink: def(res.bannerLink, ''),
        buttonColor: def(res.buttonColor, ''),
        linkDarker: def(res.linkDarker, false),
        fontColor: def(res.fontColor, '#FFFFFF'),
        gif1Link: def(res.gif1Link, ''),
        gif2Link: def(res.gif2Link, ''),
        mainLink: def(res.mainLink, ''),
        mainText: def(res.mainText, ''),
        importText: def(res.importText, ''),
        notesText: def(res.notesText, ''),
        parent: def(res.parent, '/'),
        created: res.created.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
        updated: res.updated.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow' }),
      };
      response.type('json');
      return response.send(JSON.stringify(temp));
    });
  } else {
    return response.status(400)
      .type('text')
      .send('Отсутствует необходимое поле id.');
  }
});

app.post('/tools/list/document.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }
  // Создание нового документа или перемещение старого
  // Возможные поля:
  // id - айди документа
  // parent - где создать документ
  // move - новый родитель

  if (request.body.id) {
    let o_id;
    try {
      o_id = new mongodb.ObjectID(request.body.id);
    } catch (e) {
      return response.status(400)
        .type('text')
        .send('Некорректный id.');
    }
    const args = {};

    // Нам нужна локация мува, если мы получили ID
    if (!request.body.move) {
      return response.status(400)
        .type('text')
        .send('Отсутствует необходимое поле move.');
    }
    args.updated = new Date();
    if (request.body.move === '/') {
      args.parent = '/';
      documents.updateOne({ _id: o_id }, { $set: args }).then(res => {
        return response.send(JSON.stringify(res));
      });
    } else {
      args.parent = request.body.move;
      let move_id;
      try {
        move_id = new mongodb.ObjectID(request.body.move);
      } catch (e) {
        return response.status(400)
          .type('text')
          .send('Некорректный id места назначения.');
      }
      directories.findOne({ _id: move_id }).then(res1 => {
        if (!res1) {
          return response.status(404)
            .type('text')
            .send('Не найдена папка, в которую нужно переместить.');
        }
        documents.updateOne({ _id: o_id }, { $set: args }).then(res => {
          return response.send(JSON.stringify(res));
        });
      });
    }
  } else {
    // Если нам не дали ID, это создание нового документа
    if (!request.body.name) {
      return response.status(400)
        .type('text')
        .send('Запрос должен содержать одно из необходимых полей: id, name.');
    }
    const args = {
      name: request.body.name,
      parent: request.body.parent ? request.body.parent : '/',
      created: new Date(),
      updated: new Date(),
    };

    const allowedFields = ['header', 'preheader', 'bannerLink', 'buttonColor',
      'linkDarker', 'fontColor', 'gif1Link', 'gif2Link', 'mainLink', 'mainText', 'importText', 'notesText'];

    for (const [key, value] of Object.entries(request.body)) {
      if (allowedFields.includes(key)) {
        args[key] = value;
      }
    }

    documents.insertOne(args).then(res => {
      return response.send(JSON.stringify(res));
    }).catch(e => { return response.status(500).send(JSON.stringify({ code: e })); });
  }
});

app.delete('/tools/list/document.json', jsonParser, (request, response) => {
  // Удаляет документ под означенным ID
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }
  if (!request.body.id) {
    return response.status(400)
      .type('text')
      .send('Отсутствует необходимое поле id.');
  };

  let o_id;
  try {
    o_id = new mongodb.ObjectID(request.body.id);
  } catch (e) {
    return response.status(400)
      .type('text')
      .send('Некорректный id.');
  }

  documents.deleteOne({ _id: o_id }).then(res => {
    return response.status(200).send(JSON.stringify(res));
  }).catch(e => {
    return response.status(500).send(JSON.stringify({ code: e }));
  });
});

app.patch('/tools/list/document.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }
  if (!request.body.id) {
    return response.status(400)
      .type('text')
      .send('Отсутствует необходимое поле id.');
  }

  const allowedFields = ['name', 'header', 'preheader', 'bannerLink', 'buttonColor',
    'linkDarker', 'fontColor', 'gif1Link', 'gif2Link', 'mainLink', 'mainText', 'importText', 'notesText'];

  let o_id;
  try {
    o_id = new mongodb.ObjectID(request.body.id);
  } catch (e) {
    return response.status(400)
      .type('text')
      .send('Некорректный id.');
  }

  const args = {};

  for (const [key, value] of Object.entries(request.body)) {
    if (allowedFields.includes(key)) {
      args[key] = value;
    }
  }

  args.updated = new Date();

  documents.updateOne({ _id: o_id }, { $set: args })
    .then(res => {
      // Рассылаем всем клиентам апдейт
      const responseText = JSON.stringify({
        request: 'update',
        uniqueID: request.body.uniqueID,
        documentID: request.body.id,
        ...args,
      });
      aWss.clients.forEach(client => client.send(responseText));
      return response.send(JSON.stringify(res));
    })
    .catch(e => {
      return response.status(500).send(JSON.stringify({ code: e }));
    });
});

app.get('/tools/settings/colors.json', (request, response) => {
  // Запрос списка частых цветов
  const result = [];
  settings.colors.find({}).sort({ orderNum: 1 }).toArray()
    .then(res => {
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'color',
          name: def(el.name, 'Цвет №' + el.orderNum),
          color: def(el.color, '#111111'),
          font: def(el.font, '#FFFFFF'),
          darkerLinks: def(el.darkerLinks, false),
          orderNum: el.orderNum,
        };
        result.push(temp);
      });
      response.responseType = 'json';
      return response.send(JSON.stringify(result));
    })
    .catch(e => {
      response.status(500)
        .type('json')
        .send(JSON.stringify(e));
    });
});

app.post('/tools/settings/colors.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }

  if (!Array.isArray(request.body)) {
    return response.status(400)
      .type('text')
      .send('Запрос должен быть массивом объектов, содержащих данные для внесения в БД.');
  }

  const allowedFields = ['name', 'color', 'font', 'darkerLinks', 'orderNum'];

  const insArr = [];
  request.body.forEach(el => {
    const args = {
      updated: new Date(),
    };
    for (const [key, value] of Object.entries(el)) {
      if (allowedFields.includes(key)) {
        args[key] = value;
      }
    }
    insArr.push(args);
  });

  settings.colors.deleteMany({})
    .then(() => settings.colors.insertMany(insArr))
    .then(res => {
      return response.send(JSON.stringify(res));
    })
    .catch(e => {
      return response.status(500).send(JSON.stringify(e));
    });
});

app.get('/tools/settings/tags.json', (request, response) => {
  // Запрос всех-всех-всех тегов
  const result = [];
  settings.tagsPaired.find({}).sort({ orderNum: 1 }).toArray()
    .then(res => {
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'paired',
          tag: el.tag,
          button: def(el.button, ''),
          icon: def(el.icon, ''),
          start: def(el.start, ''),
          end: def(el.end, ''),
          takesArgument: def(el.takesArgument, false),
          argAlt: def(el.argAlt, ''),
          isContent: def(el.isContent, false),
          description: def(el.description, ''),
          protected: def(el.protected, false),
          orderNum: el.orderNum,
        };
        result.push(temp);
      });
      return settings.tagsSingle.find({}).sort({ orderNum: 1 }).toArray();
    })
    .then(res => {
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'single',
          tag: el.tag,
          button: def(el.button, ''),
          icon: def(el.icon, ''),
          start: def(el.start, ''),
          takesArgument: def(el.takesArgument, false),
          argAlt: def(el.argAlt, ''),
          isContent: def(el.isContent, false),
          description: def(el.description, ''),
          protected: def(el.protected, false),
          orderNum: el.orderNum,
        };
        result.push(temp);
      });
      response.responseType = 'json';
      return response.send(JSON.stringify(result));
    })
    .catch(e => {
      response.status(500)
        .type('json')
        .send(JSON.stringify(e));
    });
});

app.get('/tools/settings/tagsPaired.json', (request, response) => {
  // Запрос парных тегов
  const result = [];
  settings.tagsPaired.find({}).sort({ orderNum: 1 }).toArray()
    .then(res => {
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'paired',
          tag: el.tag,
          button: def(el.button, ''),
          icon: def(el.icon, ''),
          start: def(el.start, ''),
          end: def(el.end, ''),
          takesArgument: def(el.takesArgument, false),
          argAlt: def(el.argAlt, ''),
          isContent: def(el.isContent, false),
          description: def(el.description, ''),
          protected: def(el.protected, false),
          orderNum: el.orderNum,
        };
        result.push(temp);
      });
      response.responseType = 'json';
      return response.send(JSON.stringify(result));
    })
    .catch(e => {
      response.status(500)
        .type('json')
        .send(JSON.stringify(e));
    });
});

app.get('/tools/settings/tagsSingle.json', (request, response) => {
  // Запрос непарных тегов
  const result = [];
  settings.tagsSingle.find({}).sort({ orderNum: 1 }).toArray()
    .then(res => {
      res.forEach(el => {
        const temp = {
          id: el._id.toString(),
          type: 'single',
          tag: el.tag,
          button: def(el.button, ''),
          icon: def(el.icon, ''),
          start: def(el.start, ''),
          takesArgument: def(el.takesArgument, false),
          argAlt: def(el.argAlt, ''),
          isContent: def(el.isContent, false),
          description: def(el.description, ''),
          protected: def(el.protected, false),
          orderNum: el.orderNum,
        };
        result.push(temp);
      });
      response.responseType = 'json';
      return response.send(JSON.stringify(result));
    })
    .catch(e => {
      response.status(500)
        .type('json')
        .send(JSON.stringify(e));
    });
});

app.post('/tools/settings/tagsPaired.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }

  if (!Array.isArray(request.body)) {
    return response.status(400)
      .type('text')
      .send('Запрос должен быть массивом объектов, содержащих id и данные для внесения в БД.');
  }

  // Собираем список записей, которые не нужно удалять
  const ids = [];
  request.body.forEach(el => {
    // Чекаем id
    if (el.id && ObjectID.isValid(el.id)) {
      let id;
      try {
        id = new mongodb.ObjectID(el.id);
      } catch (e) {
        return;
      }
      ids.push(id);
    }
  });

  // Удаляем все не защищённые записи
  settings.tagsPaired.deleteMany({
    $and: [
      {
        protected: {
          $not: { $eq: true },
        },
      },
      {
        _id: { $nin: ids },
      },
    ],
  })
    .then(() => {
      // Инициализируем массовое заполнение
      const bulk = settings.tagsPaired.initializeUnorderedBulkOp();
      const allowedFields = ['tag', 'button', 'icon', 'takesArgument', 'isContent', 'description',
        'start', 'end', 'argAlt', 'protected', 'orderNum'];
      // Выбираем все валидные id из переданного
      request.body.forEach(el => {
        // Временный объект, хранящий обновления для пуша на сервер
        const args = {
          updated: new Date(),
        };
        // Начинаем заполнять объект валидными полями
        for (const [key, value] of Object.entries(el)) {
          if (allowedFields.includes(key)) {
            args[key] = value;
          }
        }
        // Конвертация в булевы выражения
        if ('takesArgument' in args) {
          args.takesArgument = castToBool(args.takesArgument);
        }
        if ('isContent' in args) {
          args.isContent = castToBool(args.isContent);
        }
        if ('protected' in args) {
          args.protected = castToBool(args.protected);
        }
        // Конвертация тегов в массив
        if ('tag' in args && !Array.isArray(args.tag)) {
          args.tag = args.tag.split(',').map(item => item.trim());
        }
        // Проверяем ID
        if (el.id && ObjectID.isValid(el.id)) {
          // Чекаем id
          let id;
          try {
            id = new mongodb.ObjectID(el.id);
          } catch (e) {
            bulk.insert(args);
          }

          // Добавляем операцию в список
          bulk.find({ _id: id }).upsert().updateOne({ $set: args });
        } else {
          // Добавляем операцию в список
          bulk.insert(args);
        }
      });
      return bulk.execute();
    })
    .then(res => {
      return response.send(JSON.stringify(res));
    })
    .catch(e => {
      return response.status(500).send(JSON.stringify(e));
    });
});

app.post('/tools/settings/tagsSingle.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }

  if (!Array.isArray(request.body)) {
    return response.status(400)
      .type('text')
      .send('Запрос должен быть массивом объектов, содержащих id и данные для внесения в БД.');
  }

  // Собираем список записей, которые не нужно удалять
  const ids = [];
  request.body.forEach(el => {
    // Чекаем id
    if (el.id && ObjectID.isValid(el.id)) {
      let id;
      try {
        id = new mongodb.ObjectID(el.id);
      } catch (e) {
        return;
      }
      ids.push(id);
    }
  });

  // Удаляем все не защищённые записи
  settings.tagsSingle.deleteMany({
    $and: [
      {
        protected: {
          $not: { $eq: true },
        },
      },
      {
        _id: { $nin: ids },
      },
    ],
  })
    .then(() => {
      // Инициализируем массовое заполнение
      const bulk = settings.tagsSingle.initializeUnorderedBulkOp();
      const allowedFields = ['tag', 'button', 'icon', 'takesArgument', 'isContent', 'description',
        'start', 'argAlt', 'protected', 'orderNum'];
      // Выбираем все валидные id из переданного
      request.body.forEach(el => {
        // Временный объект, хранящий обновления для пуша на сервер
        const args = {
          updated: new Date(),
        };
        // Начинаем заполнять объект валидными полями
        for (const [key, value] of Object.entries(el)) {
          if (allowedFields.includes(key)) {
            args[key] = value;
          }
        }
        // Конвертация в булевы выражения
        if ('takesArgument' in args) {
          args.takesArgument = castToBool(args.takesArgument);
        }
        if ('isContent' in args) {
          args.isContent = castToBool(args.isContent);
        }
        if ('protected' in args) {
          args.protected = castToBool(args.protected);
        }
        // Конвертация тегов в массив
        if ('tag' in args && !Array.isArray(args.tag)) {
          args.tag = args.tag.split(',').map(item => item.trim());
        }
        // Проверяем ID
        if (el.id && ObjectID.isValid(el.id)) {
          // Чекаем id
          let id;
          try {
            id = new mongodb.ObjectID(el.id);
          } catch (e) {
            bulk.insert(args);
          }

          // Добавляем операцию в список
          bulk.find({ _id: id }).upsert().updateOne({ $set: args });
        } else {
          // Добавляем операцию в список
          bulk.insert(args);
        }
      });
      return bulk.execute();
    })
    .then(res => {
      return response.send(JSON.stringify(res));
    })
    .catch(e => {
      return response.status(500).send(JSON.stringify(e));
    });
});

app.patch('/tools/settings/tagsPaired.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }

  if (!Array.isArray(request.body)) {
    return response.status(400)
      .type('text')
      .send('Запрос должен быть массивом объектов, содержащих id и данные для внесения в БД.');
  }

  // Инициализируем массовое заполнение
  const bulk = settings.tagsPaired.initializeUnorderedBulkOp();
  const allowedFields = ['tag', 'button', 'icon', 'takesArgument', 'isContent', 'description',
    'start', 'end', 'argAlt', 'protected', 'orderNum'];

  request.body.forEach(el => {
    // Проверяем id
    let id;
    if (el.id && ObjectID.isValid(el.id)) {
      // Чекаем id
      try {
        id = new mongodb.ObjectID(el.id);
      } catch (e) {
        console.log(e);
        return;
      }
    } else {
      // Если id нет или он не верен, переходим к следующему элементу
      return;
    }
    // Собираем данные во временный объект
    const args = {
      updated: new Date(),
    };
    for (const [key, value] of Object.entries(el)) {
      if (allowedFields.includes(key)) {
        args[key] = value;
      }
    }
    bulk.find({ _id: id }).updateOne({ $set: args });
  });

  // Отправляем изменения
  bulk.execute()
    .then(res => {
      return response.send(JSON.stringify(res));
    })
    .catch(e => {
      return response.status(500).send(JSON.stringify(e));
    });
});

app.patch('/tools/settings/tagsSingle.json', jsonParser, (request, response) => {
  if (!request.body) {
    return response.status(400)
      .type('text')
      .send('Запрос должен содержать JSON.');
  }

  if (!Array.isArray(request.body)) {
    return response.status(400)
      .type('text')
      .send('Запрос должен быть массивом объектов, содержащих id и данные для внесения в БД.');
  }

  // Инициализируем массовое заполнение
  const bulk = settings.tagsSingle.initializeUnorderedBulkOp();
  const allowedFields = ['tag', 'button', 'icon', 'takesArgument', 'isContent', 'description',
    'start', 'argAlt', 'protected', 'orderNum'];

  request.body.forEach(el => {
    // Проверяем id
    let id;
    if (el.id && ObjectID.isValid(el.id)) {
      // Чекаем id
      try {
        id = new mongodb.ObjectID(el.id);
      } catch (e) {
        console.log(e);
        return;
      }
    } else {
      // Если id нет или он не верен, переходим к следующему элементу
      return;
    }
    // Собираем данные во временный объект
    const args = {
      updated: new Date(),
    };
    for (const [key, value] of Object.entries(el)) {
      if (allowedFields.includes(key)) {
        args[key] = value;
      }
    }
    bulk.find({ _id: id }).updateOne({ $set: args });
  });

  // Отправляем изменения
  bulk.execute()
    .then(res => {
      return response.send(JSON.stringify(res));
    })
    .catch(e => {
      return response.status(500).send(JSON.stringify(e));
    });
});

// app.listen(8089);
