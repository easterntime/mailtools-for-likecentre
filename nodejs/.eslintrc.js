module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: [
    'standard',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never',
    }],
    semi: ['error', 'always'],
    indent: ['error', 2],
    // allow async-await
    'generator-star-spacing': 'off',
    'object-curly-spacing': ['error', 'always'],
    'no-trailing-spaces': ['error'],
    'space-before-function-paren': 0,
    'no-await-in-loop': 'error',
    eqeqeq: ['error', 'smart'],
    camelcase: 0,
    'prefer-promise-reject-errors': 0,
  },
};
