// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

const editArea = document.getElementById('mainTextArea');
const ResultArea = document.getElementById('resultArea');
const PreviewArea = document.getElementById('previewArea');
const ImportArea = document.getElementById('importTextArea');
const NotesArea = document.getElementById('notesArea');

const inputBannerLink = document.getElementById('inputBannerLink');
const inputButtonColor = document.getElementsByName('inputButtonColor');
const inputButtonColorCustom = document.getElementById('inputButtonColorCustom');
const inputDarkerLinks = document.getElementById('inputDarkerLinks');
const inputFontColor = document.getElementsByName('inputFontColor');
const inputGif1 = document.getElementById('inputGif1');
const inputGif2 = document.getElementById('inputGif2');
const inputMainLink = document.getElementById('inputMainLink');
const inputHeader = document.getElementById('inputHeader');
const inputPreheader = document.getElementById('inputPreheader');
const inputName = document.getElementById('inputName');

const colorCustomPreview = document.getElementById('colorCustomPreview');

const buttonCopy = document.getElementById('copy');
const buttonDownload = document.getElementById('download');

// Флаг показывает, что документ наконец загрузился
let loaded = false;

// Переменные для подстановки в документ
const values = {
  bannerLink: '',
  buttonColor: '#FF2441',
  linkDarker: false,
  linkColor: '#FF2441',
  fontColor: '#FFF',
  gif1Link: '',
  gif2Link: '',
  mainLink: '',
  header: '',
  preheader: '',
};
let documentID = '';

const templates = {};
const templateShortcuts = {
  list: [],
  listStart: [],
  listEnd: [],
  contentTags: [],
};

let settings = {
  highlightCode: true,
};

// Если true, то мы находимся в тестовом режиме
// и сетевые функции должны быть отключены
let demoMode = true;

// Кидает необработанную ошибку
// const testError = function() {
//   // let a = ass;
//   return new Promise((resolve, reject) => {
//     reject(new Error('Unhandled Rejection, yay!'));
//   }).catch(e => handleCatch(e));
//   // return new Promise((resolve,reject) => {
//   // return resolve('Answer1');
//   // }).then(data => {
//   // return data + ' Answer2';
//   // });
// };

// Если переменная пустая, ставит значение по умолчанию
const def = function(a, defaultValue) {
  return a == null ? defaultValue : a;
};

// Если переменная пустая, возвращает false
const exists = function(value) {
  return value != null;
};

// Функция получает печеньку по имени.
// Удобнее чем каждый раз писать свой регэкс
// Источник: https://stackoverflow.com/a/25346429/13952888
function getCookie(name) {
  function escape(s) { return s.replace(/([.*+?\^$(){}|\[\]\/\\])/g, '\\$1'); }
  const match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
  return match ? match[1] : null;
}

// Закидывает кастомный HTML в iframe
const replaceIframeContent = function(iframeElement, newHTML) {
  iframeElement.src = 'about:blank';
  // iframeElement.srcdoc = newHTML;
  // iframeElement.contentWindow.location.reload(true);
  iframeElement.contentWindow.document.open();
  iframeElement.contentWindow.document.write(newHTML);
  iframeElement.contentWindow.document.close();
};

// Оборачивает текст инпута в тег
const mark = function(code) {
  const sel = window.getSelection();
  // Выделение заключено внутри нужного нам div'а?
  if (!editArea.contains(sel.anchorNode)) { return; }
  // let selectedText = sel.toString();
  const range = sel.getRangeAt(0);
  // range.deleteContents();
  // range.insertNode(document.createTextNode('[' + code + ']' + selectedText + '[/' + code + ']'));
  range.insertNode(document.createTextNode('[' + code + ']'));
  const range2 = range.cloneRange();
  range2.collapse(false);
  const newnode = document.createTextNode('[/' + code + ']');
  range2.insertNode(newnode);
  range.setEndAfter(newnode);

  goSubmit();
};

// Заменяет выделенный текст на переданное значение
const markNoBB = function(code) {
  const sel = window.getSelection();
  // Выделение заключено внутри нужного нам div'а?
  if (!editArea.contains(sel.anchorNode)) { return; }
  const range = sel.getRangeAt(0);
  range.deleteContents();
  range.insertNode(document.createTextNode(code));

  goSubmit();
};

// Оборачивает выделенный текст в настоящий HTML-тег
const markHTML = function(elClass = '', elStyle = 'background-color:yellow;') {
  const sel = window.getSelection();
  // Выделение заключено внутри нужного нам div'а?
  if (!editArea.contains(sel.anchorNode)) { return; }
  const range = sel.getRangeAt(0);
  const newNode = document.createElement('span');
  newNode.classList = elClass;
  newNode.style = elStyle;
  range.surroundContents(newNode);

  goSubmit();

  return newNode;
};

// Эта функция санитайзит поля ввода
// Соурс: https://stackoverflow.com/a/48226843/13952888
const sanitize = function(string) {
  const map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '/': '&#x2F;',
  };
  return string.replace(/[^\p{L}\p{N}\p{P}\p{Z}{\^\$}]/gu, '').replace(/[&<>"'/]/ig, (match) => (map[match]));
};

// const miniSanitize = function(string) {
//   const map = {
//     '&': '&amp;',
//     '<': '&lt;',
//     '>': '&gt;',
//     '"': '&quot;',
//     "'": '&#x27;',
//     '/': '&#x2F;',
//   };
//   return string.replace(/[&<>"'/]/ig, (match) => (map[match]));
// };

// Эта функция подставляет переменные в шаблон
// Аргументы: обрабатываемый шаблон, аргумент, словарь с переменными
const processTemplate = function(template, arg) {
  // Первым мы всегда заменяем аргумент -
  // он может содержать другие переменные
  template = template.replaceAll('$argument$', arg);

  // Перебираем все энтри словаря, и реплейсим
  // for (const [key, value] of Object.entries(values)) {
  // template.replaceAll('$'+key+'$', value);
  // }

  // Альтернативный код
  template = template.replace(/\$(bannerLink|buttonColor|linkColor|fontColor|gif1Link|gif2Link|mainLink|header|preheader)\$/gi,
    (match, p1) => values[p1]);

  return template;
};

// Создаёт в интерфейсе новую кнопку для загруженного темплейта
const newTagButton = function(item) {
  // Создаём новую кнопку
  const newButton = document.createElement('button');
  newButton.classList.add('button');
  // Тег, который будет добавляться
  const mainTag = Array.isArray(item.tag) ? item.tag[0] : item.tag;
  // Добавляем обработчики
  if (item.type === 'paired') {
    newButton.addEventListener('click', () => mark(mainTag));
  } else {
    newButton.addEventListener('click', () => markNoBB(mainTag));
  }
  // Внутренний текст, иконка
  if (item.icon) {
    if (item.button) {
      newButton.innerHTML = '<span class="icon"><i class="' + item.icon + '"></i></span><span>' + item.button + '</span>';
    } else {
      newButton.innerHTML = '<span class="icon is-small"><i class="' + item.icon + '"></i></span>';
    }
  } else {
    if (item.button) {
      newButton.innerHTML = item.button;
    } else {
      newButton.innerHTML = mainTag;
    }
  }
  // Подсказка
  newButton.title = item.description;

  // Добавляем в родителя
  document.getElementById('tagButtons').appendChild(newButton);
};

// Функция срабатывает при изменении полей ввода
const editElementChange = function(eventstuff = undefined) {
  // Достаём значения ссылочек и прочего
  values.bannerLink = encodeURI(inputBannerLink.value.trim());
  // Цвет кнопок, включая кастомный
  inputButtonColor.forEach((el) => {
    if (el.checked) {
      values.buttonColor = el.value;
    }
  });
  if (values.buttonColor === 'custom') {
    // Туду: валидация, а не санитизация
    values.buttonColor = sanitize(inputButtonColorCustom.value.trim());
    if (values.buttonColor.search('#') !== 0) {
      values.buttonColor = '#' + values.buttonColor;
    }
  }
  // Затемнение цвета шрифта ссылок
  if (inputDarkerLinks.checked) {
    values.linkColor = pSBC(-0.4, values.buttonColor);
  } else {
    values.linkColor = values.buttonColor;
  }
  // Цвет шрифта
  inputFontColor.forEach((el) => {
    if (el.checked) {
      values.fontColor = el.value;
    }
  });
  values.gif1Link = encodeURI(inputGif1.value.trim());
  if (values.gif1Link.search(/media\d{0,3}\.giphy\.com/gmi) > -1) {
    if (values.gif1Link.search(/^.+?giphy\.gif/gmi) > -1) {
      values.gif1Link = values.gif1Link.replace(/^(.+?giphy\.gif).*/gmi, '$1');
    }
  }
  values.gif2Link = encodeURI(inputGif2.value.trim());
  if (values.gif2Link.search(/media\d{0,3}\.giphy\.com/gmi) > -1) {
    if (values.gif2Link.search(/^.+?giphy\.gif/gmi) > -1) {
      values.gif2Link = values.gif2Link.replace(/^(.+?giphy\.gif).*/gmi, '$1');
    }
  }

  values.mainLink = encodeURI(inputMainLink.value.trim());
  values.header = inputHeader.value.trim();
  values.preheader = inputPreheader.value.trim();

  if (eventstuff) {
    goSubmit();
  }
};

// Основная функция обработки данных
// complete перезагружает все данные полностью
const goSubmit = function(complete = false) {
  // Если ещё не загружены шаблоны, ничего не трогаем
  if (!loaded) { return; }

  // Копируем телеса
  let lines = editArea.innerText
    .replace(/\p{Emoji}/ug, (m, idx) => he.encode(m))
  // List, Greyarea, Card, Button etc. должны быть всегда на отдельной строчке
    .replace(templateShortcuts.regexContentStart, '\n$&')
    .replace(templateShortcuts.regexContentEnd, '$&\n')
    .split(/\r?\n/);

  if (complete) {
    editElementChange();
  }

  // Кэшируем теги
  for (const key in templates) {
    if (Object.prototype.hasOwnProperty.call(templates, key)) {
      templates[key].startCache = processTemplate(templates[key].start, templates[key].argAlt);
      if (templates[key].type === 'paired') {
        templates[key].endCache = processTemplate(templates[key].end, templates[key].argAlt);
      }
    }
  }

  // Начинаем обработку
  // Флаги контекста
  let list = false;
  // const banner = false;
  // let grey = false;

  let max1 = lines.length;
  for (let i = 0; i < max1; i++) {
    if (lines[i].search(/\[List.\]/gi) > -1) { list = true; }
    if (lines[i].search(/\[\/List.\]/gi) > -1) { list = false; }

    // Расставим абзацы
    // Игнорируем пустые строки
    if ((lines[i].trim() != '') && (lines[i].search(templateShortcuts.regexContentTags) < 0)) {
      if (!list) {
        // Если это значащий абзац и не список
        lines[i] = templates.p.startCache + lines[i] + templates.p.endCache;
      } else {
        // Итем списка
        lines[i] = templates.listitem.startCache + lines[i] + templates.listitem.endCache;
      }
    }
  }

  // Теперь глобальные замены
  // тут же присоединяется баннер
  let result = lines.join('\n');

  // Расставляем шаблончики
  result = result.replace(templateShortcuts.regexStart, (match, p1) => {
    // Пытаемся угадать шаблон по тегу
    let tag;
    // Регэкс для извлечения тега из структуры вида [tag] или [tag="somevalue"]
    const pureTag = p1.match(/(?<=\[).*?(?=\]|=)/i);
    if ((pureTag != null) && (pureTag[0].toLowerCase() in templates)) {
      tag = templates[pureTag[0].toLowerCase()];
    } else if (p1.toLowerCase() in templates) {
      tag = templates[p1.toLowerCase()];
    } else {
      console.log(match);
      return '';
    }
    if (tag.takesArgument) {
      const arg = match.match(/(?<==").*?(?=")/i);
      if (arg != null) {
        return processTemplate(tag.start, arg[0]);
      } else {
        return tag.startCache;
      }
    } else {
      return tag.startCache;
    }
  }).replace(templateShortcuts.regexEnd, (match, p1) => {
    // Пытаемся угадать шаблон по тегу
    let tag;
    // Регэкс для извлечения тега из структуры вида [/tag] или [/tag="somevalue"]
    const pureTag = p1.match(/(?<=\[\/).*?(?=\]|=)/i);
    if ((pureTag != null) && (pureTag[0].toLowerCase() in templates)) {
      tag = templates[pureTag[0].toLowerCase()];
    } else if (p1.toLowerCase() in templates) {
      tag = templates[p1.toLowerCase()];
    } else {
      console.log(match);
      return '';
    }
    return tag.endCache;
  });

  // Вторая прогонка, чтобы правильно расставить пробелы
  lines = result
    .replace(/(?:\r?\n){3,}/gm, '\n\n')
    .split(/\r?\n/);
  max1 = lines.length;
  for (i = 0; i < max1; i++) {
    lines[i] = '                ' + lines[i];
  }

  result = lines.join('\n');

  // Вывод результата и превью
  ResultArea.value = templates.document.startCache + result + templates.document.endCache;

  replaceIframeContent(PreviewArea, templates.document.startCache + result + templates.document.endCache);

  // PreviewArea.innerHTML = previewStart + result + previewEnd;
};

const goImport = function() {
  let theText = ImportArea.value;

  // Удаляем комментарии
  theText = theText.replace(/<!--(.|[\r\n])*?-->/gmi, '')
    .replace(/<p style="color: red; font-weight: bold">.*<\/p>/gmi, '')
    .replace(/<ul style="color: red; font-weight: bold">.*<\/ul>/gmi, '')
  // Фикс ошибки с h1, h2
    .replace(/<h[12]>/gmi, '')
    .replace(/<\/h[12]>/gmi, '')
  // Меняем <br> на переносы строки
    .replace(/<\/?br\s?\/?>/gmi, '\n')
  // Удаляем неделимые пробелы
    .replaceAll('&nbsp;', ' ')
  // Удаляем пробелы в начале и конце строк
    .replaceAll(/^[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]+/gmi, '')
    .replaceAll(/[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]+$/gmi, '')
  // Удаляем пустые строки
    .replace(/^\n\s*/gmi, '')
  // Меняем начало и конец абзаца на теги
    .replace(/<p.*?>\w*\n/gmi, '')
    .replace(/\n\w*<\/p.*?>/gmi, '')
  // Ловим осиротевшие [P],[/P]
  // theText = theText.replace(/(\[P\].*?)(?<!\[\/P\])$/gmi,'$1[/P]');
  // theText = theText.replace(/^(?!\[P\])(.*?\[\/P\])/gmi,'[P]$1');
  // Гифки
    .replace(/^.*giphy\.com.*$/mi, '[Gif1]')
    .replace(/^.*giphy\.com.*$/mi, '[Gif2]')
  // Жирный шрифт
    .replace(/<(strong|b)>\n*/gmi, '[B]')
    .replace(/\n*<\/(strong|b)>/gmi, '[/B]')
  // Италик
    .replace(/<(em|i)>\n*/gmi, '[I]')
    .replace(/\n*<\/(em|i)>/gmi, '[/I]')
  // Подчёркивание (ссылка)
    .replace(/<span.*?>\s*/gmi, '[Link]')
    .replace(/\s*<\/span.*?>/gmi, '[/Link]')
  // Пункты списка (удаляются)
    .replace(/<\/?li>/gmi, '')
  // Сам список (дефолтится в А)
    .replace(/<ul.*?>\w*/gmi, '[ListA]')
    .replace(/<\/ul.*?>\w*/gmi, '[/ListA]')
    .replace(/<ol.*?>\w*/gmi, '[ListB]')
    .replace(/<\/ol.*?>\w*/gmi, '[/ListB]')
  // Если список у нас из чёрточек
    .replace(/(^-\s?(.|[\r\n])*?)\n(?!-)/gmi, '[ListA]\n$1\n[/ListA]\n')
    .replace(/^-\s?.*?/gmi, '')
  // Кнопки
    .replace(/\*(.*?)\*(?: ссылка| https?:\/\S*)?/mi, '[Btn]$1[/Btn]')
    .replace(/\*(.*?)\*(?: ссылка| https?:\/\S*)?/gmi, '[Btn2]$1[/Btn2]')
  // Удаляем лишние ссылки
    .replace(/^<a.*<\/a>\$/gmi, '')
  // Пробелы перед тегами - в топку
    .replace(/(?=.*) (?=\[\/|$)/gmi, '')
  // И добавить после тегов там, где это кошерно
    .replace(/(\[\/([IB]|Link)\])(?!\[|\s)/gmi, '$1 ');
  // Выдираем заголовок
  if (theText.search(/\[P\](Заголовок|Тема):\s.*?\[\/P\]/m) > -1) {
    try {
      inputHeader.value = theText.match(/(?:Заголовок|Тема):\s.*?$/m)[0]
        .replace(/(?:Заголовок|Тема):\s(.*?)$/m, '$1');
      inputPreheader.value = theText.match(/(?:Прех|Прехедер|Подзаголовок):\s.*?$/m)[0]
        .replace(/(?:Прех|Прехедер|Подзаголовок):\s(.*?)$/m, '$1');
      sendCustom({
        header: inputHeader.value,
        preheader: inputPreheader.value,
      });
    } catch (e) {
      console.log(e, 'Опять заголовки не поймались...');
    }
  }
  theText = theText.replace(/(?:Заголовок|Тема|Прех|Прехедер|Подзаголовок):\s.*?$/gmi, '')
  // "Привет!"
    .replace(/^Привет!$/m, '[B]$&[/B]')
  // Красивые переносики
    .replace(/(?!^)\n(?!\s*\n)/gmi, '\n\n');
  // Бонус: неделимые пробелы перед смайликами и между числами
  // theText = theText.replace(/\s(:\)|;\))/gmi,'&nbsp;$1');
  // theText = theText.replace(/(\d+)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]{1}(\d+)/gmi,'$1&nbsp;$2');
  // theText = theText.replace(/(\d+|млн\.?|тыс\.?)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000](млн\.?|тыс\.?|р\.|руб\.?|₽)/gmi,'$1&nbsp;$2')

  editArea.innerText = theText;
  goSubmit();
};

// Обрабатывает текст, чтобы он был готов к публикации в емейле
const makeGood = function() {
  const typograph = function(theText, escapeAmp) {
    // Конвертируем эскейпнутые символы в настоящие
    theText = theText.replaceAll('&#38;', '&')
      .replaceAll('&amp;', '&')

      .replace(emojiRegex, (m, idx) => he.encode(m))

    // 14. Несколько простых пробелов подряд сливаются в один.
      .replace(/[ ]{2,}/gmi, ' ')

    // 2. Перед смайликами и эмодзи ставятся неделимые пробелы.
    // Не могу сделать пробелы перед эмодзи. Не знаю, как это сделать, чтобы ничего не поломать.
      .replace(/(?<!&nbsp;)\s?([:;][)(|/\\*])/gmi, '&nbsp;$1')

    // 3. Между каждым числом и следующим числом или словом ставится неделимый пробел.
      .replace(/(\d+)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000](?!%)/gmi, '$1&nbsp;')

    // Исключение: перед знаком процента '%' ставится короткий неделимый пробел.
      .replace(/(\d+)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]?(?=%)/gmi, '$1&#8239;')

    // 4. Минусы "-", отбитые двумя пробелами или только пробелом справа, становятся тире, отбитым пробелами с двух сторон. Левый - неделимый.
    // 5. Длинное тире между двумя словами отбивается неделимым пробелом с левой стороны и обычным - справа.
      .replace(/(?:&nbsp;|\s?)(?:-|—)\s/gmi, '&nbsp;— ')

    // 6. Минус или тире между двумя числами, не отбитый пробелами, превращается в короткое тире.
    // Короткое тире разрывается, а разрывать нельзя. Вот ведь зараза.
    // Сначала заменяем на числовое тире
      .replace(/(?<=\d+)[-–—‐](?=\d+)/gmi, '&#8210;')
    // Затем объединяем всё число в неразрывную группу
      .replace(/(?<=^|\s)((?:\d)+(?:&#8210;)+\S+)/gmi, '[nowrap]$1[/nowrap]')

    // 7. Минус или дефис между двумя словами, не отбитый пробелами, превращается в неделимый дефис.
      .replace(/(?<=\s(?!\[)[A-Za-zА-Яа-я]*(?!\[)[A-Za-zА-Яа-я]+)[-‐](?=\S)/gmi, '&#8209;')

    // 8. Все слова длиной 1 или 2 буквы приклеиваются неделимым пробелом к следующему слову. Исключения: "ли", "бы", "же", "и".
      .replace(/((?<=^|\s|&nbsp;)(?!ли|бы|же|и\s)[а-я*]{1,2})(?!$)\s/gmi, '$1&nbsp;')

    // 9. Все предлоги, с которых начинается предложение, приклеиваются к следующему слову.
      .replace(/(?<=Без|Безо|Близ|В|Во|Вместо|Вне|Для|До|За|Из|Изо|Из-за|Из-под|К|Ко|Кроме|Между|Меж|На|Над|Надо|О|Об|Обо|От|Ото|Перед|Передо|Пред|Предо|Пo|Под|Подо|При|Про|Ради|С|Со|Сквозь|Среди|У|Через|Чрез)\s/gm, '&nbsp;')

    // 10. "Ли" и "себя" приклеиваются вместо этого к предыдущему слову, если они не первые в предложении.
      .replace(/(?<!^)\s(?=(?:себя|ли|бы|же)[^А-яA-z])/gmi, '&nbsp;')

    // 13. Три точки подряд превращаются в символ троеточия.
      .replace(/\.{3}/gmi, '&#8230;')

    // 15. Убираются пробелы перед знаками препинания.
      .replace(/\s([.,!?;:])/gmi, '$1')

    // 11. Длинные числа, отбитые с левой стороны пробелом, разбиваются по три знака справа налево. Единица измерения отбивается неделимым пробелом. Пример: ' 1000000₽' -> ' 1 000 000 ₽'.
      .replace(/\d+(?=руб|р\.|₽|тыс|млн|миллионов|млрд|миллиардов)/gmi, '$&&nbsp;')
      .replace(/(?<=\s|&nbsp;|^)\d{4,}(?=\s|&nbsp;|$)/gmi, (sMatch) => {
        // Проверка на актуальные даты. Потому что утомил
        if (sMatch > 1979 && sMatch < 2041) { return sMatch; }
        let result = '';
        const len = sMatch.length;
        const high = Math.floor((len - 1) / 3) - 1;
        for (let i = 0; i <= high; i++) {
          result = '&nbsp;' + sMatch.slice(len - (i * 3) - 3, len - (i * 3)) + result;
        }
        return sMatch.slice(0, len - (high + 1) * 3) + result;
      })

    // 12. Программистские кавычки " ... " меняются на французские ёлочки « ... ».  Так и не придумал, как быстро реализовать вложенные кавычки.
      .replace(/(?<!\[\S*)"(?=[А-яA-z])/gmi, '«')
      .replace(/(?<=[А-яA-z])"(?!\S*])/gmi, '»');

    // Другие части речи - неделимый пробел ПОСЛЕ
    // (я|мы|ты|вы|он|она|оно|они|мой|твой|ваш|наш|свой|его|ее|её|их|весь|кто|что|какой|каков|чей|сколько|никто|ничто|некого|нечего|никакой|ничей|нисколько||||||||||||||||||||||||||||||||)

    if (settings.showEntities && escapeAmp) {
      theText = theText.replace(/&(#?.{1,6};)/gmi, '&#38;$1');
    }

    return theText;
  };

  // Номер задачи: 294619

  const emojiRegex = /\p{Emoji}/ug;
  // /([^\p{L}\p{N}\p{P}\p{Z}{\^\$}])/gu;
  // /\p{Emoji}/ug;

  // Типографим:
  // Заголовки
  values.header = inputHeader.value = typograph(inputHeader.value, false);
  values.preheader = inputPreheader.value = typograph(inputPreheader.value, false);

  // Основной текст
  editArea.innerHTML = typograph(editArea.innerHTML, true);
  goSubmit();
};

// Эта функция копирует в буфер обмена содержимое textfield'а "Результат"
const goCopy = function() {
  // Добавляем анимацию загрузки
  buttonCopy.classList.add('is-loading');

  // Отправляем текст на копирование
  const data = ResultArea.value;
  navigator.clipboard.writeText(data).then(() => {
    // Убираем анимацию загрузки
    buttonCopy.classList.remove('is-loading');
    // Добавляем галочку
    buttonCopy.innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Скопировано!</span>';
    // Возвращаем кнопку в нормальное состояние через 2 секунды
    setTimeout(() => {
      buttonCopy.innerHTML = 'Скопировать результат';
    }, 2000);
  }).catch(e => handleCatch(e));
};

// Эта функция скачивает весь проект как файл
const goDownload = function() {
  // Отправляем текст на копирование
  const data = {};
  data.importText = importTextArea.value;
  data.mainText = editArea.innerHTML;
  data.bannerLink = values.bannerLink;
  data.buttonColor = values.buttonColor;
  data.linkColor = values.linkColor;
  data.fontColor = values.fontColor;
  data.gif1Link = values.gif1Link;
  data.gif2Link = values.gif2Link;
  data.mainLink = values.mainLink;
  data.header = values.header;
  data.preheader = values.preheader;

  download(JSON.stringify(data, null, 2), 'export.json');
};

// Подсветка кода
// Clear очищает подсветку, но не ставит новую
const goHighlight = function(clear = false) {
  if (!settings.highlightCode && !clear) { return; }
  // Сохраним выделение
  // let range = window.getSelection().getRangeAt(0);
  // let rangeSave = {
  // startNode: range.startContainer,
  // startOffset: range.startOffset,
  // endOffset: range.endOffset
  // };
  try {
    const restore = saveSelection(document);
    // Выберем все строки
    const strings = editArea.children;
    for (const p of strings) {
      const highlights = p.getElementsByClassName('highlight');
      while (highlights.length > 0) {
        highlights[0].outerHTML = highlights[0].innerHTML;
      }
      if (!clear) {
        p.innerHTML = p.innerHTML.replace(templateShortcuts.regexStart, '<span class="highlight" style="color:#005194; font-weight:600;">$&</span>').replace(templateShortcuts.regexEnd, '<span class="highlight" style="color:#005194; font-weight:600;">$&</span>');
      }
    }
    // Возвращаем выделение
    // range.setStart(rangeSave.startNode,rangeSave.startOffset);
    // range.setEnd(rangeSave.startNode,rangeSave.endOffset);
    restoreSelection(document, restore);
  } catch (e) {
    console.log(e);
  }
};

function saveSelection(containerEl) {
  const range = window.getSelection().getRangeAt(0);
  const preSelectionRange = range.cloneRange();
  preSelectionRange.selectNodeContents(containerEl);
  preSelectionRange.setEnd(range.startContainer, range.startOffset);
  const start = preSelectionRange.toString().length;

  return {
    start,
    end: start + range.toString().length,
  };
}

function restoreSelection(containerEl, savedSel) {
  let charIndex = 0; const range = document.createRange();
  range.setStart(containerEl, 0);
  range.collapse(true);
  const nodeStack = [containerEl]; let node; let foundStart = false; let stop = false;

  while (!stop && (node = nodeStack.pop())) {
    if (node.nodeType == 3) {
      const nextCharIndex = charIndex + node.length;
      if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
        range.setStart(node, savedSel.start - charIndex);
        foundStart = true;
      }
      if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
        range.setEnd(node, savedSel.end - charIndex);
        stop = true;
      }
      charIndex = nextCharIndex;
    } else {
      let i = node.childNodes.length;
      while (i--) {
        nodeStack.push(node.childNodes[i]);
      }
    }
  }

  const sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
}

// Эта функция ставит дефолтные настройки цвета шрифта ссылок
// при выборе цвета кнопок
const colorChange = function(fontColor, darkerLinks) {
  inputDarkerLinks.checked = darkerLinks;

  inputFontColor.forEach((el) => {
    if (el.value == fontColor) {
      el.checked = true;
    }
  });

  values.linkDarker = darkerLinks;
  values.fontColor = fontColor;

  sendCustom({
    linkDarker: values.linkDarker,
    fontColor: values.fontColor,
  });
};

// Эта функция ограничивает частоту срабатывания автоматического предпросмотра
// https://towardsdev.com/debouncing-and-throttling-in-javascript-8862efe2b563
const throttle = function(func, limit) {
  let lastFunc;
  let lastRan;
  return function() {
    const context = this;
    const args = Array.prototype.slice.call(arguments, 2);
    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function() {
        if ((Date.now() - lastRan) >= limit) {
          func.apply(context, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
};

// Эта функция позволяет срабатывание функции только после окончания инпута
const debounce = function(func, delay) {
  let timer;
  return function () { // anonymous function
    const context = this;
    const args = arguments;
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(context, args);
    }, delay);
  };
};

// Эта функция позволяет скачать текст как файл
// Соурс: https://stackoverflow.com/a/34156339/13952888
const download = function (content, fileName, contentType = 'text/plain') {
  const a = document.createElement('a');
  const file = new Blob([content], { type: contentType });
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
};

// Загружает темплейты с сервера
const updateTemplates = () => new Promise((resolve, reject) => {
  // Эскейпит все регексовые спецсимволы
  const regexSanitize = function(string) {
    return string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  };

  // Получение ответа от сервера
  fetchGet('/tools/settings/tags.json', {})
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      if (!Array.isArray(data)) {
        // WTF
        console.log(data);
        return;
      }
      // Перебираем теги
      const temp = {
        start: [],
        end: [],
        contentStart: [],
        contentEnd: [],
      };
      data.forEach(el => {
        // Добавляем в список
        if (Array.isArray(el.tag)) {
          // Если теги - массив, перебираем все теги
          el.tag.forEach(el1 => {
            // В теге эскейпим опасные для регэкса символы
            const elTag = regexSanitize(el1);
            // Темплейт
            templates[el1.toLowerCase()] = el;
            if (el.type === 'paired') {
              temp.start.push('\\[' + elTag + (el.takesArgument ? '(?:=".*?")?\\]' : '\\]'));
              temp.end.push('\\[\\/' + elTag + '\\]');
              if (el.isContent) {
                temp.contentStart.push('\\[' + elTag + '\\]');
                temp.contentEnd.push('\\[\\/' + elTag + '\\]');
              }
            } else {
              temp.start.push(elTag);
              if (el.isContent) {
                temp.contentStart.push(elTag);
                temp.contentEnd.push(elTag);
              }
            }
          });
        } else {
          const elTag = regexSanitize(el.tag);
          templates[el.tag.toLowerCase()] = el;
          if (el.type === 'paired') {
            temp.start.push('\\[' + elTag + (el.takesArgument ? '(?:=".*?")?\\]' : '\\]'));
            temp.end.push('\\[\\/' + elTag + '\\]');
            if (el.isContent) {
              temp.contentStart.push('\\[' + elTag + '\\]');
              temp.contentEnd.push('\\[\\/' + elTag + '\\]');
            }
          } else {
            temp.start.push(elTag);
            if (el.isContent) {
              temp.contentStart.push(elTag);
              temp.contentEnd.push(elTag);
            }
          }
        }

        // Кнопка
        if (el.button !== 'hidden') {
          newTagButton(el);
        }
      });
      // Регэксы
      templateShortcuts.regexStart = new RegExp('(' + temp.start.join('|') + ')', 'gmi');
      templateShortcuts.regexEnd = new RegExp('(' + temp.end.join('|') + ')', 'gmi');
      templateShortcuts.regexContentTags = new RegExp('(' + temp.contentStart.concat(temp.contentEnd).join('|') + ')', 'gmi');
      templateShortcuts.regexContentStart = new RegExp('(' + temp.contentStart.join('|') + ')', 'gmi');
      templateShortcuts.regexContentEnd = new RegExp('(' + temp.contentEnd.join('|') + ')', 'gmi');
      return resolve(true);
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
});

// Загружает цвета с сервера
const updateColors = () => new Promise((resolve, reject) => {
  // Получение ответа от сервера
  fetchGet('/tools/settings/colors.json', {})
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      if (!Array.isArray(data)) {
        // WTF
        console.log(data);
        return;
      }
      // Добавляем все цвета в интерфейс
      const controlDiv = document.getElementById('colors');
      data.forEach(el => {
        const newNode = document.createElement('label');
        newNode.classList = 'radio';
        newNode.dataset.id = el.id;
        newNode.innerHTML = '<input type="radio" class="mainEditElement" name="inputButtonColor" value="' + el.color + '"><img class="colorPreview" style="margin: 0 3px; background-color: ' + el.color + ';">' + el.name;
        newNode.addEventListener('click', () => {
          colorChange(el.font, el.darkerLinks);
        });
        controlDiv.appendChild(newNode);
      });
      // Кастомный цвет
      const newNode = document.createElement('label');
      newNode.classList = 'radio';
      newNode.dataset.id = el.id;
      newNode.innerHTML = '<input id="inputButtonColor9" name="inputButtonColor" type="radio" class="mainEditElement" value="custom" style="margin-right: 2px;">' + 'Свой';
      newNode.addEventListener('click', () => {
        colorChange(el.font, el.darkerLinks);
      });
      controlDiv.appendChild(newNode);
      // Отмечаем первый цвет в списке как активный
      controlDiv.getElementsByTagName('input')[0].checked = true;
      return resolve(true);
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
});

// Загружает документ с сервера
const refresh = () => new Promise((resolve, reject) => {
  // Превью цвета
  colorCustomPreview.style.color = inputButtonColorCustom.value.charAt(0) === '#' ? inputButtonColorCustom.value : '#' + inputButtonColorCustom.value;
  // Получаем айди
  const urlParams = new URLSearchParams(window.location.search);
  if (!urlParams.has('id')) {
    // Если id нет, то мы открыли редактор в тестовом режиме.
    demoMode = true;
    return resolve(false);
  }
  documentID = urlParams.get('id');
  fetchGet('/tools/list/document.json', { id: documentID })
    .then(res => {
      if (res.status == 404) {
        // Если нет такого файла, уходим в demoMode
        history.pushState({}, '', window.location.origin + window.location.pathname);
        demoMode = true;
        throw new Error('Файл с таким ID не найден.');
      }
      // Хэндлинг ошибок от сервера
      return handleCodes(res);
    })
    .then(res => res.json())
    .then(data => {
      document.getElementById('breadcrumbName').innerText = data.name;
      inputName.value = data.name;
      demoMode = false;
      ImportArea.value = data.importText;
      editArea.innerHTML = data.mainText;
      NotesArea.value = data.notesText;
      values.bannerLink = data.bannerLink;
      inputBannerLink.value = data.bannerLink;
      values.buttonColor = data.buttonColor;
      let succ = false;
      // Совпадение со списком цветов
      inputButtonColor.forEach((el) => {
        if (succ) { return; }
        if (el.value == values.buttonColor) {
          el.checked = true;
          succ = true;
        } else {
          el.checked = false;
        }
      });
      if (!succ) {
        // Кастомное значение
        inputButtonColor[inputButtonColor.length - 1].checked = true;
        inputButtonColorCustom.value = data.buttonColor;
      }
      // Превью цвета
      colorCustomPreview.style.color = data.buttonColor.charAt(0) === '#' ? data.buttonColor : '#' + data.buttonColor;
      // Остальное
      values.linkDarker = data.linkDarker;
      inputDarkerLinks.checked = data.linkDarker;
      values.fontColor = data.fontColor;
      inputFontColor.forEach((el) => {
        el.checked = (el.value == data.fontColor);
      });
      values.gif1Link = data.gif1Link;
      inputGif1.value = data.gif1Link;
      values.gif2Link = data.gif2Link;
      inputGif2.value = data.gif2Link;
      values.mainLink = data.mainLink;
      inputMainLink.value = data.mainLink;
      values.header = data.header;
      inputHeader.value = data.header;
      values.preheader = data.preheader;
      inputPreheader.value = data.preheader;
      return resolve(true);
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
});

// Отправляет изменения в реалтайме на сервер
// Вероятно будет переписано позже на websockets
const sendChanges = function(context) {
  if (demoMode) { return; }
  const dict = {
    importTextArea: 'importText',
    mainTextArea: 'mainText',
    inputBannerLink: 'bannerLink',
    inputGif1: 'gif1Link',
    inputGif2: 'gif2Link',
    inputMainLink: 'mainLink',
    inputHeader: 'header',
    inputPreheader: 'preheader',
    inputDarkerLinks: 'linkDarker',
  };
  const params = {
    id: documentID,
    uniqueID,
  };
  const paramsWS = {
    session: getCookie('token'),
    uniqueID,
    documentID,
    request: 'update',
  };
  if (this.type === 'checkbox') {
    params[dict[this.id]] = this.checked;
    paramsWS[dict[this.id]] = this.checked;
  } else if (this.isContentEditable) {
    params[dict[this.id]] = this.innerHTML;
    paramsWS[dict[this.id]] = this.innerHTML;
  } else {
    params[dict[this.id]] = this.value;
    paramsWS[dict[this.id]] = this.value;
  }
  // Проверяем состояние вебсокета. Если нет соединение, фоллбэкаемся.
  if (ws.readyState === WebSocket.OPEN && wsEnabled) {
    // Уведомляем сервер об изменениях
    ws.send(JSON.stringify(paramsWS));
  } else {
    fetchUpdate('/tools/list/document.json', params)
      .then(res => handleCodes(res))
      .catch(e => {
        // Если это уже ошибка, нет смысла создавать новую ошибку
        if (e instanceof Error) {
          handleCatch(e);
        } else {
          handleCatch(new Error(JSON.stringify(e)));
        }
      });
  }
};

// То же самое, но для радио
// Я не уверен, что мы можем доверять клику для определения значения
const sendChangesRadio = function(context) {
  if (demoMode) { return; }
  const radioSet = document.getElementsByName(this.name);

  const dict = {
    inputButtonColor: 'buttonColor',
    inputFontColor: 'fontColor',
  };
  const params = {
    id: documentID,
    uniqueID,
  };
  const paramsWS = {
    session: getCookie('token'),
    uniqueID,
    documentID,
    request: 'update',
  };

  for (const el of radioSet) {
    if (el.checked) {
      // Если это "Свой" цвет
      if (el.value === 'custom') {
        params[dict[this.name]] = inputButtonColorCustom.value;
        paramsWS[dict[this.name]] = inputButtonColorCustom.value;
      } else {
        params[dict[this.name]] = el.value;
        paramsWS[dict[this.name]] = el.value;
      }
      break;
    }
  }

  params[dict[this.id]] = this.value;
  paramsWS[dict[this.id]] = this.value;
  // Уведомляем сервер об изменениях
  if (ws.readyState === WebSocket.OPEN && wsEnabled) {
    ws.send(JSON.stringify(paramsWS));
  } else {
    fetchUpdate('/tools/list/document.json', params)
      .then(res => handleCodes(res))
      .catch(e => {
        // Если это уже ошибка, нет смысла создавать новую ошибку
        if (e instanceof Error) {
          handleCatch(e);
        } else {
          handleCatch(new Error(JSON.stringify(e)));
        }
      });
  }
};

// Отправка программных изменений, не по клику
const sendCustom = function(params) {
  if (demoMode) { return; }
  const paramsWS = { ...params };
  paramsWS.session = getCookie('token');
  paramsWS.uniqueID = uniqueID;
  paramsWS.documentID = documentID;
  paramsWS.request = 'update';
  params.id = documentID;
  params.uniqueID = uniqueID;
  params.request = 'update';
  if (ws.readyState === WebSocket.OPEN && wsEnabled) {
    ws.send(JSON.stringify(paramsWS));
  } else {
    fetchUpdate('/tools/list/document.json', params)
      .then(res => handleCodes(res))
      .catch(e => {
        // Если это уже ошибка, нет смысла создавать новую ошибку
        if (e instanceof Error) {
          handleCatch(e);
        } else {
          handleCatch(new Error(JSON.stringify(e)));
        }
      });
  }
};

// Открывает модальную форму создания UTM-ссылок
const wrapOpen = function() {
  // Открываем форму
  document.getElementById('modalWrap').classList.add('is-active');
  // Парсим имеющуюся ссылку
  const url = new URL(inputMainLink.value);
  // Основная ссылка, без квери
  document.getElementById('modalWrapLink').value = url.origin + url.pathname;
  // Номер задачи
  if (url.searchParams.has('utm_content')) {
    document.getElementById('modalWrapTask').value = url.searchParams.get('utm_content');
  } else {
    // Пробуем предположить - выбираем из задачи подходящий номер
    const match = inputName.value.match(/\d{5,8}/);
    if (match) {
      document.getElementById('modalWrapTask').value = match;
    }
  }
  // Тип рассылки
  if (url.searchParams.has('utm_term')) {
    document.getElementById('modalWrapType').value = url.searchParams.get('utm_term');
  } else {
    // Пробуем предположить - выбираем из задачи подходящий тайтл
    // let matched = false;
    // let name = inputName.value;
    // let dict = {
    // 'marketing': 'crm_marketing',
    // 'annonce': 'annonce_kp',
    // 'service': 'service_kp',
    // 'native': 'native_kp',
    // 'emergency': 'emergency',
    // 'auto': 'autoemail'
    // };
    // for (const [key, value] of Object.entries(dict)) {
    // if (name.includes(key)) {
    // matched = true;
    // document.getElementById('modalWrapType').value = value;
    // break;
    // }
    // }
    // if (!matched) {
    // document.getElementById('modalWrapType').value = 'crm_marketing';
    // }
    document.getElementById('modalWrapType').value = 'crm_marketing';
  }
  // Продукт
  if (url.searchParams.has('utm_product')) {
    document.getElementById('modalWrapProduct').value = url.searchParams.get('utm_product');
  } else {
    // Пробуем предположить - выбираем из задачи подходящий продукт
    let matched = false;
    const name = inputName.value;
    ['concentrat', 'reality', 'speed', 'sotka', 'strategy', 'msa', 'event'].forEach(el => {
      if (name.includes(el)) {
        matched = true;
        document.getElementById('modalWrapProduct').value = el;
      }
    });
  }

  wrapChange();
};

// Оборачивает ссылку в UTM-метки и выводит в превью
const wrapChange = function() {
  const origin = document.getElementById('modalWrapLink').value.trim();
  const url = new URL(origin);
  url.searchParams.set('utm_campaign', origin.substr(origin.lastIndexOf('/') + 1));
  url.searchParams.set('utm_medium', 'email');
  url.searchParams.set('utm_source', 'emarsys');
  url.searchParams.set('utm_content', document.getElementById('modalWrapTask').value.trim());
  url.searchParams.set('utm_term', document.getElementById('modalWrapType').selectedOptions[0].value.trim());
  url.searchParams.set('utm_product', document.getElementById('modalWrapProduct').selectedOptions[0].value.trim());

  document.getElementById('modalWrapPreview').value = url.toString();
  // https://mestum.com/events/sotka6?utm_campaign=sotka6&utm_medium=email&utm_source=emarsys&utm_content=email_1610_1900&utm_term=service_kp&utm_product=sotka
};

// Ставит ссылку из превью в основную ссылку
const wrapAccept = function() {
  values.mainLink = document.getElementById('modalWrapPreview').value;
  inputMainLink.value = values.mainLink;
  this.closest('.modal').classList.remove('is-active');
  sendCustom({ mainLink: values.mainLink });
};

// Создаёт объект для отправки
const dataForSave = function(parent = null, id = null) {
  const result = {};
  if (id) {
    result.id = id;
  }
  if (parent) {
    result.parent = parent;
  }
  result.name = inputName.value.trim();
  result.mainText = editArea.innerHTML.trim();
  result.notesText = NotesArea.value.trim();
  result.bannerLink = values.bannerLink = inputBannerLink.value.trim();
  // Цвет кнопок, включая кастомный
  inputButtonColor.forEach((el) => {
    if (el.checked) {
      values.buttonColor = el.value;
    }
  });
  if (values.buttonColor === 'custom') {
    // Туду: валидация, а не санитизация
    values.buttonColor = sanitize(inputButtonColorCustom.value.trim());
    if (values.buttonColor.search('#') !== 0) {
      values.buttonColor = '#' + values.buttonColor;
    }
  }
  result.buttonColor = values.buttonColor;
  result.linkDarker = values.linkDarker = inputDarkerLinks.checked;
  inputFontColor.forEach((el) => {
    if (el.checked) {
      values.fontColor = el.value;
    }
  });
  result.fontColor = values.fontColor;
  result.gif1Link = values.gif1Link = inputGif1.value.trim();
  result.gif2Link = values.gif2Link = inputGif2.value.trim();
  result.mainLink = values.mainLink = inputMainLink.value.trim();
  result.header = values.header = inputHeader.value.trim();
  result.preheader = values.preheader = inputPreheader.value.trim();
  return result;
};

let treeview;

// Открывает модальную форму сохранения файла
// Если он уже сохранён, то делаем ручной полный сейв
const saveOpen = function() {
  // Рекурсивная функция создания дерева
  const setChildren = function(node, source) {
    source.children.forEach(el => {
      const newchild = new TreeNode(el.name);
      newchild.changeOption('id', el._id);
      node.addChild(newchild);
      setChildren(newchild, el);
    });
  };

  // Если имя пустое, выходим
  if (!inputName.value.trim()) {
    return;
  }

  if (demoMode) {
    fetchGet('/tools/list/directorylist.json', {})
      .then(res => handleCodes(res))
      .then(res => res.json())
      .then(data => {
        const root = new TreeNode('root', { id: '/' });
        if (treeview) {
          // Очистка всех нод
          // root = treeview.getRoot();
          // root.getChildren().map(el => {
          // root.removeChild(el);
          // })
          treeview.setRoot(root);
        } else {
          // Создание нового дерева
          treeview = new TreeView(root, document.getElementById('treeview'));
          treeview.changeOption('leaf_icon', '<i class="fas fa-folder"></i>');
          treeview.changeOption('parent_icon', '<i class="fas fa-folder"></i>');
        }
        // Формируем дерево
        setChildren(root, data);
        root.setSelected(true);
        treeview.reload();
        document.getElementById('modalSave').classList.add('is-active');
      })
      .catch(e => {
        // Если это уже ошибка, нет смысла создавать новую ошибку
        if (e instanceof Error) {
          handleCatch(e);
        } else {
          handleCatch(new Error(JSON.stringify(e)));
        }
      });
  } else {
    // Добавляем анимацию загрузки
    const saveButton = document.getElementById('saveButton');
    saveButton.classList.add('is-loading');
    // Постим запрос
    fetchUpdate('/tools/list/document.json', dataForSave(null, documentID))
      .then((res) => {
        // Убираем анимацию загрузки
        saveButton.classList.remove('is-loading');
        // eslint-disable-next-line eqeqeq
        if (res.status == 200) {
          // Добавляем галочку
          saveButton.innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Сохранено!</span>';
        } else {
          // Добавляем крестик
          saveButton.innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка! Код: ' + res.status + '</span>';
          console.log(res.body);
        }
        // Возвращаем кнопку в нормальное состояние через 2 секунды
        setTimeout(() => {
          saveButton.innerHTML = 'Сохранить';
        }, 2000);
      })
      .catch(e => {
        // Убираем анимацию загрузки
        saveButton.classList.remove('is-loading');
        // Добавляем крестик
        saveButton.innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка!</span>';
        handleCatch(e);
        // Возвращаем кнопку в нормальное состояние через 2 секунды
        setTimeout(() => {
          saveButton.innerHTML = 'Сохранить';
        }, 2000);
      });
  }
};

// Сохранение нового документа
const saveAccept = function() {
  fetchPost('/tools/list/document.json', dataForSave(treeview.getSelectedNodes()[0].getOptions().id, null))
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      if (!data.insertedId) {
        console.log(data);
        throw new Error('Поле не было вставлено.');
      }
      const urlParams = new URLSearchParams(window.location.search);
      urlParams.set('id', data.insertedId);
      history.pushState({ id: data.insertedId }, '', window.location.origin + window.location.pathname + '?' + urlParams.toString());
      demoMode = false;
      documentID = data.insertedId;
      document.getElementById('modalSave').classList.remove('is-active');
    })
    .catch(e => {
      // Если это уже ошибка, нет смысла создавать новую ошибку
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Открывает диалог создания новой папки
const newDirOpen = function() {
  document.getElementById('modalNewDirName').value = '';
  document.getElementById('modalNewDir').classList.add('is-active');
};

// Создаёт новую папку
const newDirAccept = function() {
  const aParent = treeview.getSelectedNodes()[0];
  const name = document.getElementById('modalNewDirName').value;
  // aParentID = aParent.getOptions().id;
  fetchPost('/tools/list/directory.json', {
    name,
    parent: aParent.getOptions().id,
  })
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      const newChild = new TreeNode(name, { id: data.insertedId });
      aParent.addChild(newChild);
      aParent.setSelected(false);
      newChild.setSelected(true);
      treeview.reload();
      document.getElementById('modalNewDir').classList.remove('is-active');
    })
    .catch(e => {
      // Если это уже ошибка, нет смысла создавать новую ошибку
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

const noteCreate = function() {
  // Создаём выделение
  const newNode = markHTML('note');
  // Относительно уникальный айди
  newNode.id = 'note' + (Date.now()).toString() + (Math.random()).toString();
  // Кто создал запись
  newNode.dataset.author = window.localStorage.getItem('login');
  // Когда запись создали
  newNode.dataset.datetime = new Date().toLocaleString('ru-RU', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    timeZone: 'Europe/Moscow',
  });
  // Текст записи
  newNode.dataset.note = '';
  // Обработчик клика
  newNode.addEventListener('click', noteOpen);
  sendCustom({ mainText: editArea.value });
  newNode.click();
};

const noteOpen = function() {
  const card = document.getElementById('noteCard');
  card.dataset.id = this.id;
  document.getElementById('notePoster').innerText = this.dataset.author;
  document.getElementById('noteDate').innerText = this.dataset.datetime;
  document.getElementById('noteText').value = this.dataset.note;
  const bodyRect = document.body.getBoundingClientRect();
  const rect = this.getBoundingClientRect();
  card.style.left = (+rect.left + +rect.width) + 'px';
  card.style.top = (+rect.top + +rect.height - bodyRect.top) + 'px';
  // card.style.left = (+rect.left + +rect.width) + "px";
  // card.style.top = (+rect.top + +rect.height) + "px";
  card.classList.remove('is-hidden');
};

const noteDel = function() {
  const card = document.getElementById('noteCard');
  const el = document.getElementById(card.dataset.id);
  el.outerHTML = el.innerHTML;
  card.classList.add('is-hidden');
};

const noteSave = function() {
  const card = document.getElementById('noteCard');
  const cardText = document.getElementById('noteText');
  const el = document.getElementById(card.dataset.id);
  el.dataset.note = cardText.value;
  sendCustom({ mainText: editArea.value });
  card.classList.add('is-hidden');
};

// Часть, где мы занимаемся вебсокетами
const wsURL = 'wss://vrcare.ru/tools/ws';
const statusElement = document.getElementById('navbarUsers');
const uniqueID = (Date.now()).toString() + (Math.random()).toString();
let ws;
let wsTimeToReconnect = 1000;
let wsTimer;
let wsEnabled = false;

const wsConnect = function(firstConnect = false) {
  ws = new WebSocket(wsURL);

  // По открытию соединения
  ws.onopen = () => {
    console.log(new Date().toLocaleTimeString('ru'), 'Socket is connected successfully.');
    statusElement.innerText = 'Подключено.';
    wsTimeToReconnect = 1000;
    wsEnabled = true;
    // Запросить обновление
    ws.send(JSON.stringify({
      session: getCookie('token'),
      uniqueID,
      documentID,
      request: 'handshake',
    }));
  };

  // Обработка сообщений, основная функция
  ws.onmessage = (msg) => {
    console.log(msg.data);
    const data = JSON.parse(msg.data);
    // Если это ошибка
    if (data.request === 'error') {
      handleCatch(new Error(JSON.stringify(data.msg)));
    }
    // Если это сообщение о количестве пользователей
    else if (data.request === 'users' && exists(data.count)) {
      let word = '';
      if (data.count === 1) {
        word = ' пользователь.';
      } else if (data.count <= 4) {
        word = ' пользователя.';
      } else {
        word = ' пользователей.';
      }

      statusElement.innerText = 'Подключено. На сайте ' + data.count + word;
    }
    // Если это апдейт и если это наш документ и сообщение отправили не мы
    else if (data.request === 'update' && data.documentID === documentID && data.uniqueID !== uniqueID) {
      if (exists(data.importText)) {
        ImportArea.value = data.importText;
      }
      if (exists(data.mainText)) {
        editArea.innerHTML = data.mainText;
      }
      if (exists(data.notesText)) {
        NotesArea.value = data.notesText;
      }
      if (exists(data.bannerLink)) {
        values.bannerLink = data.bannerLink;
        inputBannerLink.value = data.bannerLink;
      }
      if (exists(data.buttonColor)) {
        let succ = false;
        // Совпадение со списком цветов
        inputButtonColor.forEach((el) => {
          if (succ) { return; }
          if (el.value === values.buttonColor) {
            el.checked = true;
            succ = true;
          } else {
            el.checked = false;
          }
        });
        if (!succ) {
          // Кастомное значение
          inputButtonColor[inputButtonColor.length - 1].checked = true;
          inputButtonColorCustom.value = data.buttonColor;
          // Превью цвета
          colorCustomPreview.style.color = data.buttonColor.charAt(0) === '#' ? data.buttonColor : '#' + data.buttonColor;
        }
      }
      if (exists(data.linkDarker)) {
        values.linkDarker = data.linkDarker;
        inputDarkerLinks.checked = data.linkDarker;
      }
      if (exists(data.fontColor)) {
        values.fontColor = data.fontColor;
        inputFontColor.forEach((el) => {
          el.checked = (el.value === data.fontColor);
        });
      }
      if (exists(data.gif1Link)) {
        values.gif1Link = data.gif1Link;
        inputGif1.value = data.gif1Link;
      }
      if (exists(data.gif2Link)) {
        values.gif2Link = data.gif2Link;
        inputGif2.value = data.gif2Link;
      }
      if (exists(data.mainLink)) {
        values.mainLink = data.mainLink;
        inputMainLink.value = data.mainLink;
      }
      if (exists(data.header)) {
        values.header = data.header;
        inputHeader.value = data.header;
      }
      if (exists(data.header)) {
        values.header = data.header;
        inputHeader.value = data.header;
      }
      if (exists(data.preheader)) {
        values.preheader = data.preheader;
        inputPreheader.value = data.preheader;
      }
    }
  };

  // При закрытии соединения нужно отправляться на реконнект
  ws.onclose = (msg) => {
    console.log(new Date().toLocaleTimeString('ru'), 'Socket is closed. Reconnect will be attempted in 1 second.', msg.reason);
    statusElement.innerText = 'Переподключение...';
    wsEnabled = false;
    if (!wsTimer) {
      wsTimer = setTimeout(() => {
        // Повышаем время до повторного реконнекта
        wsTimeToReconnect = wsTimeToReconnect >= 60000 ? 60000 : wsTimeToReconnect + 2000;
        // Пытаемся реконнектнуться
        wsConnect();
        wsTimer = false;
      }, wsTimeToReconnect);
    }
  };

  ws.onerror = (err) => {
    console.error(new Date().toLocaleTimeString('ru'), 'Socket encountered error: ', err.message, 'Closing socket');
    wsEnabled = false;
    if (!wsTimer) {
      wsTimer = setTimeout(() => {
        // Повышаем время до повторного реконнекта
        wsTimeToReconnect = wsTimeToReconnect >= 60000 ? 60000 : wsTimeToReconnect + 2000;
        // Пытаемся реконнектнуться
        wsConnect();
        wsTimer = false;
      }, wsTimeToReconnect);
    }
  };
};

// Инициализация. Жирная функция
const ready = function() {
  // Подцепим листенер к аккордеону
  // bulmaCollapsible.attach('.is-collapsible',{allowMultiple: true});

  // Фикс айфрейма под Firefox
  replaceIframeContent(PreviewArea, '');

  // Кнопочки
  document.getElementById('makeGood').addEventListener('click', makeGood, false);

  // Загружаем теги с сервера
  // updateTemplates();
  Promise.all([
    updateTemplates(),
    updateColors(),
  ]).then(() => refresh())
    .then(() => {
      loaded = true;
      // Подключаемся к вебсокетс
      wsConnect(true);
      goHighlight();
      goSubmit();
      const temp = document.getElementsByClassName('note');
      for (const el of temp) {
        el.addEventListener('click', noteOpen);
      }
    });

  // document.getElementById('collapsible-message-accordion-meta-1').element.style.height = this.element.scrollHeight + 'px'

  // Добавляем листенеры к элементам страницы
  const editElement = document.getElementsByClassName('mainEditElement');

  editArea.addEventListener('input', throttle(goSubmit, 2000), false);
  editArea.addEventListener('input', throttle(sendChanges, 500), false);
  editArea.addEventListener('input', debounce(goHighlight, 3000), false);
  notesArea.addEventListener('input', throttle(sendChanges, 1000), false);
  for (const el of editElement) {
    if (el.type === 'radio') {
      el.addEventListener('click', editElementChange, false);
      el.addEventListener('click', sendChangesRadio, false);
    } else {
      el.addEventListener('change', editElementChange, false);
      el.addEventListener('change', sendChanges, false);
    }
  }

  // Автоматически отмечаем "Свой" цвет, если мы изменили свой цвет
  inputButtonColorCustom.addEventListener('change', function() {
    inputButtonColor[inputButtonColor.length - 1].checked = true;
    colorCustomPreview.style.color = this.value.charAt(0) === '#' ? this.value : '#' + this.value;
    sendCustom({ buttonColor: this.value });
  }, false);

  // Выделение текста, всплывающий комментарий
  document.getElementById('buttonComment').addEventListener('click', noteCreate);
  document.getElementById('noteSave').addEventListener('click', noteSave);
  document.getElementById('noteDel').addEventListener('click', noteDel);

  // Включение и выключение подсветки кода, сохранение/загрузка в хранилище
  let temp = window.localStorage.getItem('settings');
  if (temp && typeof JSON.parse(temp) === 'object') {
    settings = JSON.parse(temp);
    document.getElementById('checkboxHighlightCode').checked = settings.highlightCode;
    document.getElementById('checkboxShowEntities').checked = settings.showEntities;
  }
  document.getElementById('checkboxHighlightCode').addEventListener('click', function() {
    settings.highlightCode = this.checked;
    window.localStorage.setItem('settings', JSON.stringify(settings));
    goHighlight(!this.checked);
  }, false);
  document.getElementById('checkboxShowEntities').addEventListener('click', function() {
    settings.showEntities = this.checked;
    window.localStorage.setItem('settings', JSON.stringify(settings));
  }, false);

  // Переключение режимов предпросмотра
  document.getElementById('previewPC').addEventListener('click', function() {
    document.getElementById('previewMobile').classList.remove('is-active');
    document.getElementById('previewCustom').classList.remove('is-active');
    this.classList.add('is-active');
    document.getElementById('previewArea').style.maxWidth = 'none';
  });
  document.getElementById('previewMobile').addEventListener('click', function() {
    document.getElementById('previewPC').classList.remove('is-active');
    document.getElementById('previewCustom').classList.remove('is-active');
    this.classList.add('is-active');
    document.getElementById('previewArea').style.maxWidth = '300px';
  });
  document.getElementById('previewCustom').addEventListener('click', function() {
    document.getElementById('previewPC').classList.remove('is-active');
    document.getElementById('previewMobile').classList.remove('is-active');
    this.classList.add('is-active');
    const input = document.getElementById('previewSize');
    const width = input.value && Number.isInteger(+input.value) ? input.value + 'px' : 'none';
    document.getElementById('previewArea').style.maxWidth = width;
  });

  // Кнопки вытаскивания гифок по ссылке
  // document.getElementById("buttonGetGif1").addEventListener('click',getGif1);
  // document.getElementById("buttonGetGif2").addEventListener('click',getGif2);

  // Кнопка закрытия форм
  temp = document.getElementsByClassName('modalClose');
  for (const el of temp) {
    el.addEventListener('click', function() {
      this.closest('.modal').classList.remove('is-active');
    }, false);
  }

  document.getElementById('noteClose').addEventListener('click', function() {
    document.getElementById('noteCard').classList.add('is-hidden');
  });

  // UTM-конструктор
  document.getElementById('linkWrapButton').addEventListener('click', wrapOpen, false);
  temp = document.getElementsByClassName('modalWrapInput');
  for (const el of temp) {
    el.addEventListener('change', wrapChange, false);
  }
  document.getElementById('modalWrapAccept').addEventListener('click', wrapAccept, false);

  // Выбор папки для сохранения
  document.getElementById('saveButton').addEventListener('click', saveOpen, false);
  document.getElementById('modalSaveAccept').addEventListener('click', saveAccept, false);
  document.getElementById('modalSaveNewDir').addEventListener('click', newDirOpen, false);
  document.getElementById('newDirAccept').addEventListener('click', newDirAccept, false);
};

document.addEventListener('DOMContentLoaded', ready);

// Навбар для мобильных устройств
document.addEventListener('DOMContentLoaded', () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  }
});

// Соурс: https://stackoverflow.com/a/13542669
// Функция преобразования цвета
// Version 4.0
const pSBC = (p, c0, c1, l) => {
  let r; let g; let b; let P; let f; let t; let h; const i = parseInt; const m = Math.round; let a = typeof (c1) == 'string';
  if (typeof (p) != 'number' || p < -1 || p > 1 || typeof (c0) != 'string' || (c0[0] !== 'r' && c0[0] !== '#') || (c1 && !a)) return null;
  if (!this.pSBCr) {
    this.pSBCr = (d) => {
      let n = d.length; const x = {};
      if (n > 9) {
        [r, g, b, a] = d = d.split(','), n = d.length;
        if (n < 3 || n > 4) return null;
        x.r = i(r[3] == 'a' ? r.slice(5) : r.slice(4)), x.g = i(g), x.b = i(b), x.a = a ? parseFloat(a) : -1;
      } else {
        if (n === 8 || n === 6 || n < 4) return null;
        if (n < 6)d = '#' + d[1] + d[1] + d[2] + d[2] + d[3] + d[3] + (n > 4 ? d[4] + d[4] : '');
        d = i(d.slice(1), 16);
        if (n === 9 || n === 5)x.r = d >> 24 & 255, x.g = d >> 16 & 255, x.b = d >> 8 & 255, x.a = m((d & 255) / 0.255) / 1000;
        else x.r = d >> 16, x.g = d >> 8 & 255, x.b = d & 255, x.a = -1;
      } return x;
    };
  }
  h = c0.length > 9, h = a ? c1.length > 9 ? true : c1 == 'c' ? !h : false : h, f = pSBCr(c0), P = p < 0, t = c1 && c1 != 'c' ? pSBCr(c1) : P ? { r: 0, g: 0, b: 0, a: -1 } : { r: 255, g: 255, b: 255, a: -1 }, p = P ? p * -1 : p, P = 1 - p;
  if (!f || !t) return null;
  if (l)r = m(P * f.r + p * t.r), g = m(P * f.g + p * t.g), b = m(P * f.b + p * t.b);
  else r = m((P * f.r ** 2 + p * t.r ** 2) ** 0.5), g = m((P * f.g ** 2 + p * t.g ** 2) ** 0.5), b = m((P * f.b ** 2 + p * t.b ** 2) ** 0.5);
  a = f.a, t = t.a, f = a >= 0 || t >= 0, a = f ? a < 0 ? t : t < 0 ? a : a * P + t * p : 0;
  if (h) return 'rgb' + (f ? 'a(' : '(') + r + ',' + g + ',' + b + (f ? ',' + m(a * 1000) / 1000 : '') + ')';
  else return '#' + (4294967296 + r * 16777216 + g * 65536 + b * 256 + (f ? m(a * 255) : 0)).toString(16).slice(1, f ? undefined : -2);
};
