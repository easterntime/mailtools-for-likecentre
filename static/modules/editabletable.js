// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

// Интерактивные заполняемые таблицы с экспортом в JSON.
// Поддерживает несколько таблиц на одной странице.
// Экспорт автоматически берёт названия из заголовков таблицы, и добавляет только
// столбцы с непустыми заголовками
// Шаблон для вставки в HTML можно найти в самом низу.


//Добавление строки по кнопке
const tableAdd = function() {
	tableAddByCode(this.closest('.table-editable'))
}

//Добавление строки через код
//На самом деле копирует спрятанную строку-образец и лепит в конец tbody
//table принимает element или id таблицы
//fillObj принимает объект с парами {ключ:значение}
const tableAddByCode = function(table, fillObj = false, dataObj = false) {
	let theTableBody;
	if(table.nodeType) {
		//По элементу таблицы
		theTableBody = table.getElementsByTagName('tbody')[0];
	} else {
		//По id таблицы
		theTableBody = document.getElementById(table).getElementsByTagName('tbody')[0];
	}
	//Ищем образец и клонируем его
	let temp = theTableBody.getElementsByClassName('hide')[0].cloneNode(true);
	//Делаем видимым
	temp.classList.remove('hide');
	//Добавляем data-свойства
	//Если переданный объект не пустой
	let dataObjExists = dataObj && Object.getPrototypeOf(dataObj) === Object.prototype && Object.keys(dataObj).length > 0;
	if (dataObjExists) {
		// Добавляем все пары ключ-значение в датасет
		for (const [key,value] of Object.entries(dataObj)) {
			temp.dataset[key] = value;
		}
	}
	//Навешиваем обработчики кнопок
	//Если строка защищённая, то убираем кнопку удаления
	if (dataObjExists && dataObj.protected === true) {
		temp.getElementsByClassName('table-remove')[0].remove();
	} else {
		temp.getElementsByClassName('table-remove')[0].addEventListener('click', tableRemove, false);
	};
	temp.getElementsByClassName('table-up')[0].addEventListener('click', tableUp, false);
	temp.getElementsByClassName('table-down')[0].addEventListener('click', tableDown, false);
	//Суём в конец tbody
	theTableBody.appendChild(temp);
	
	//Если строку надо предзаполнить, отправляем в соответствующую функцию
	if(fillObj) {
		tableFill(temp,fillObj);
	}
}

//Удаление всех строк
const tableClear = function(table) {
	let theTableChildren;
	if(table.nodeType === Node.ELEMENT_NODE) {
		theTableChildren = table.querySelectorAll('tbody tr:not(.hide)');
	} else {
		theTableChildren = document.getElementById(table).querySelectorAll('tbody tr:not(.hide)');
	}
	
	for (el of theTableChildren) {
		el.remove();
	}
}

//Удаление строки
const tableRemove = function() {
	this.closest('tr').remove();
}

//Двигаем строку вверх
const tableUp = function() {
	const theTableBody = this.closest('tbody');
	//Ищем все строки
	let allRows = theTableBody.getElementsByTagName('tr');
	//Определяем нашу
	let row = this.closest('tr');
	//Индекс нашей
	let rowIndex = Array.from(allRows).indexOf(row);
	//Выше секрета двигаться нельзя
	if (rowIndex <= 1) return;
	//Двигаем предыдущую строку сразу за нашу строку
	row.after(allRows[rowIndex-1]);
}

//Двиньте вниз, гражданин
const tableDown = function() {
	const theTableBody = this.closest('tbody');
	//Ищем все строки
	let allRows = theTableBody.getElementsByTagName('tr');
	//Определяем нашу
	let row = this.closest('tr');
	//Индекс нашей
	let rowIndex = Array.from(allRows).indexOf(row);
	//Ниже низа двигать не надо
	if (rowIndex === allRows.length-1) return;
	//Двигаем за следующую строку
	row.before(allRows[rowIndex+1]);
}

// Экспорт таблицы в объект
const tableExport = function(tableID, includeDataset = true) {
	let theTableBody;
	let rowHeaders;
	if(tableID.nodeType) {
		//По элементу
		theTableBody = tableID.getElementsByTagName('tbody')[0];
		rowHeaders = tableID.getElementsByTagName('th');
	} else {
		//По id
		let myTable = document.getElementById(tableID);
		theTableBody = myTable.getElementsByTagName('tbody')[0];
		rowHeaders = myTable.getElementsByTagName('th');
	}
	const rows = theTableBody.querySelectorAll('tr:not(.hide)');
	let headers = {};
	let data = [];
	
	//Сперва перебираем заголовки
	
	//Ищем все не-пустые заголовки и суём их под правильными номерами
	//В первую очередь проверяется data-name заголовка, если его нет, мы берём его название
	for (const [index,value] of Array.from(rowHeaders).entries()) {
		//Если data-ignoreHeader == true, пропускаем этот заголовок
		if (value.dataset.ignoreHeader) {continue};
		//Если data-name задан, то мы используем именно его. Иначе - заголовок столбца
		let temp = false;
		if (value.dataset.name) {
			temp = value.dataset.name;
		} else {
			temp = value.innerText.replace(/[!@#$^&%*()+=[\]\/{}|:<>?,.\\\-\s]/g, '');
		}
		
		if(temp) {
			headers[index] = temp;
		}
	}
	
	//Мы хотим получить поля под номерами не-пустых заголовков и сунуть их в ison
	//Старт не с первого элемента!
	let counter = 0;
	for (const el of rows) {
		let rowContent = el.getElementsByTagName('td');
		if (rowContent.length<=0) {continue};
		let temp = new Object();
		for (const [index,value] of Array.from(rowContent).entries()) {
			if (!headers[index]) {continue};
			temp[headers[index]] = value.innerText.trim();
		}
		// Экспортируем датасет тоже
		if (includeDataset) {
			for (const [key,value] of Object.entries(el.dataset)) {
				temp[key] = value;
			}
			temp.orderNum = counter;
		}
		data.push(temp);
		counter++;
	}
	
	return data;
}

//Заполнение строки данными из объекта
const tableFill = function(aRow, fillObj) {
	const rowHeaders = aRow.closest('.table-editable').getElementsByTagName('th');
	let headers = {};
	let temp;
	
	for ([index,value] of Array.from(rowHeaders).entries()) {
		if (value.dataset.name) {
			temp = value.dataset.name;
		} else {
			temp = value.innerText.replace(/[!@#$^&%*()+=[\]\/{}|:<>?,.\\\-\s]/g, '');
		}
		
		if(temp) {
			headers[index] = temp;
		}
	}
	
	const fields = aRow.getElementsByTagName('td');
	
	for ([index,el] of Array.from(fields).entries()) {
		if (!headers[index]) {continue};
		if (headers[index] in fillObj) {
			el.innerText = fillObj[headers[index]];
		} else {
			el.innerText = '';
		}
	}
}

document.addEventListener('DOMContentLoaded', () => {
	let collection = document.getElementsByClassName('table-add');
	for (el of collection) {
		el.addEventListener('click', tableAdd, false);
	}
	collection = document.getElementsByClassName('table-remove');
	for (el of collection) {
		el.addEventListener('click', tableRemove, false);
	}
	collection = document.getElementsByClassName('table-up');
	for (el of collection) {
		el.addEventListener('click', tableUp, false);
	}
	collection = document.getElementsByClassName('table-down');
	for (el of collection) {
		el.addEventListener('click', tableDown, false);
	}
})


// Код для вставки в HTML

//<div id="table" class="table-editable">
//	<span class="table-add">➕</span>
//	<table class="table">
//		<!-- tbody очень важен -->
//		<tbody>
//			<!-- Строка-образец -->
//			<tr class="hide">
//				<td contenteditable="true">Значение 1</td>
//				<td contenteditable="true">Значение 2</td>
//				<td>
//					<span class="table-remove">❌</span>
//				</td>
//				<td>
//					<span class="table-up">▲</span>
//					<span class="table-down">▼</span>
//				</td>
//			</tr>
//			<!-- Заголовки -->
//			<tr>
//				<th>Столбец 1</th>
//				<th>Столбец 2</th>
//				<th></th>
//				<th></th>
//			</tr>
//			<!-- Контент -->
//			<tr class="hide">
//				<td contenteditable="true">Значение 1</td>
//				<td contenteditable="true">Значение 2</td>
//				<td>
//					<span class="table-remove">❌</span>
//				</td>
//				<td>
//					<span class="table-up">▲</span>
//					<span class="table-down">▼</span>
//				</td>
//			</tr>
//		</tbody>
//	</table>
//</div>