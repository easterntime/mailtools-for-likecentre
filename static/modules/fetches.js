// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

//Проверяет, пуст ли переданный объект
const isEmpty = function(obj) {
	if (!obj) {return true}
	for (var i in obj) {return false}
	return true
}

// Обёртка для стандартного GET-запроса
//Params должен быть {key:value} объектом
const fetchGet = function(url, params = null) {
	let fetchString = url;
	if (!isEmpty(params)) {
		fetchString += "?";
		for (const [key,value] of Object.entries(params)) {
			fetchString += encodeURI(key) + '=' + encodeURI(value) + '&';
		}
		fetchString = fetchString.slice(0, -1);
	}
	return fetch(fetchString, {
		method: 'GET',
		credentials: 'same-origin',
		redirect: 'follow'
	})
}

// Обёртка для стандартных запросов, передающих JSON
const fetchGeneric = function(url, params, type) {
	return fetch(url, {
		headers: { "Content-Type": "application/json; charset=utf-8" },
		method: type,
		credentials: 'same-origin',
		redirect: 'follow',
		body: JSON.stringify(params)
	})
}

// Обёртка для стандартного POST-запроса
const fetchPost = function(url, params = null) {
	return fetchGeneric(url, params, 'POST')
}

// Обёртка для стандартного DELETE-запроса
const fetchDelete = function(url, params = null) {
	return fetchGeneric(url, params, 'DELETE')
}

// Обёртка для стандартного PATCH-запроса
const fetchUpdate = function(url, params = null) {
	return fetchGeneric(url, params, 'PATCH')
}