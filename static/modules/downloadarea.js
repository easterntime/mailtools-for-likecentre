const downloadAreaReady = function() {
	const allForms = document.getElementsByClassName('uploadbox');

	if (isAdvancedUpload) {
		for (aForm of allForms) {
			el.classList.add('has-advanced-upload');
	
			var droppedFiles = false;

			[ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event ) {
				aForm.addEventListener( event, function( e ) {
					// preventing the unwanted behaviours
					e.preventDefault();
					e.stopPropagation();
				});
			});
			[ 'dragover', 'dragenter' ].forEach( function( event ) {
				aForm.addEventListener( event, function() {
					aForm.classList.add( 'is-dragover' );
				});
			});
			[ 'dragleave', 'dragend', 'drop' ].forEach( function( event ) {
				aForm.addEventListener( event, function() {
					aForm.classList.remove( 'is-dragover' );
				});
			});
			aForm.addEventListener( 'drop', function( e ) {
				droppedFiles = e.dataTransfer.files; // the files that were dropped
				showFiles( droppedFiles );
				
				triggerFormSubmit();

			});
		}
	}
}

document.addEventListener('DOMContentLoaded', downloadAreaReady);

const isAdvancedUpload = function() {
  let div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

