const accordionReady = function() {
	const accordionClick = document.getElementsByClassName('accordionClick');
	for (el of accordionClick) {
		el.addEventListener('click', accordionCollapse, false);
	}
	
	const accordionBodies = document.getElementsByClassName('collapsible-message');
	for (el of accordionBodies) {
		el.addEventListener('transitionend', accordionHeightToAuto, false);
	}
}

const accordionCollapse = function() {
	// Получаем из заголовка айди контейнера аккордиона
	const accordionBody = document.getElementById(this.dataset.href);
	if (accordionBody) {
		// Получаем его дочку - контент
		const accordionContent = document.querySelector('#'+this.dataset.href+' > .message-body-content');
		if (accordionContent) {
			// Высота элемента есть высота его дочки
			accordionBody.style.height = accordionContent.scrollHeight + 'px';
			// Выключаем метку активности
			accordionBody.classList.toggle('is-active');
			// Если мы сворачиваем спойлер...
			if (!accordionBody.classList.contains('is-active')) {
				//...нужно сначала подождать, пока height сменится с auto на мануальное значение
				setTimeout((theBody)=>{theBody.style.height='0'},50,accordionBody);
			} else {
				//Иначе свернём родню
				const accordionBodies = document.querySelectorAll('#'+this.dataset.parent+' .collapsible-message[data-parent="'+this.dataset.parent+'"]');
				let elChild;
				for (el of accordionBodies) {
					if ((el.classList.contains('is-active')) && (el != accordionBody)) {
						el.classList.remove('is-active');
						elChild = document.querySelector('#'+el.id+' > .message-body-content');
						if (elChild) {
							el.style.height = elChild.scrollHeight + 'px';
							setTimeout((theBody)=>{theBody.style.height='0'},50,el);
						} else {
							el.style.height = '0';
						}
					}
				}
			}
		}
	}
}

const accordionHeightToAuto = function() {
	if (this.classList.contains('is-active')) {
		this.style.height='auto';
	}
}

document.addEventListener("DOMContentLoaded", accordionReady);

// const accordionCollapse = function() {
	// // Получаем из заголовка айди контейнера аккордиона
	// const accordionBody = document.getElementById(this.dataset.href);
	
	// if (accordionBody) {
		// // Получаем его дочку - контент
		// const accordionContent = document.querySelector('#'+this.dataset.href+' > .message-body-content');
		
		// if (accordionContent) {
			// if (!accordionBody.classList.contains('is-active')) {
				// const accordionBodies = document.querySelectorAll('#'+this.dataset.parent+' .collapsible-message[data-parent="'+this.dataset.parent+'"]');
				// for (el of accordionBodies) {
					// el.classList.remove('is-active');
				// }
			// }
			// //accordionBody.style.height = accordionContent.scrollHeight + 'px';
			// accordionBody.classList.toggle('is-active');
		// }
	// }
// }