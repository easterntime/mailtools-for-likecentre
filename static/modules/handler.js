// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================


let errorMsg;
let errorBody;

// Эта функция кушает ошибку и показывает её пользователю
function handleError(err) {
	errorMsg.classList.remove("is-hidden");
	// Текст ошибки, который увидит пользователь
	let el1 = document.createElement("p");
	// Если ошибка - не объект, то мы,
	// скорее всего, имеем дело с кастомным throw
	if (typeof err === 'object') {
		let errObj = null;
		// Это обычная ошибка?
		if (err instanceof Error) {
			errObj = err;
		// Это Unhandled Rejection?
		} else if (err.reason) {
			// Это Error?
			if (err.reason instanceof Error) {
				errObj = err.reason;
			} else {
				el1.innerHTML = JSON.stringify(err.reason);
			}
		// Это Unhandled Error?
		} else if (err.error instanceof Error) {
			errObj = err.error;
		// У этого объекта есть вообще message?
		} else if (err.message) {
			el1.innerText = JSON.stringify(err.message);
		// Видимо, это какой-то левый объект
		} else {
			el1.innerText = JSON.stringify(err);
		}
		// Если мы нашли объект ошибки, вылавливаем данные именно из него
		if (errObj) {
			el1.innerHTML = (errObj.name ? '<strong>' + errObj.name + ':</strong> ' + errObj.message :
			errObj.message.replace(/^.*?(?=\s)/,'<strong>$&</strong>')) + '<br>' +
				errObj.fileName.match(/(?!.*\/).*/)[0] + ':' + errObj.lineNumber + ':' + errObj.columnNumber;
		}
	// Это вообще не объект?
	} else {
		el1.innerText = JSON.stringify(err);
	}
	errorBody.appendChild(el1);
}

// Эта функция используется в реджектах для того,
// чтобы вызванная ошибка осталась в консоли
function handleCatch(err) {
	console.log(err);
	handleError(err);
};

// Эта функция кушает ответ сервера и определяет, надо ли кидать ошибку
// Если всё ок - резольвится. Иначе кидает реджект, который нужно ловить выше по стеку.
function handleCodes(res, additionalInfo) {
	return new Promise((resolve,reject) => {
		// Если у нас даже кода ошибки нет, что-то пошло ОЧЕНЬ не так
		if (typeof res.status != 'number') {
			return reject('Критическая ошибка подключения к серверу: нет даже кода ответа!');
		}
		// Коды меньше 400, как правило, не сообщают об ошибках.
		if (res.status<400) {
			return resolve(res);
		} else {
			const dict = {
				400: 'Сервер отклонил запрос.',
				408: 'Время запроса истекло. Всё ли в порядке с интернетом?',
				417: 'Сервер отклонил запрос из-за нарушения ожиданий.',
				500: 'Внутренняя ошибка сервера!',
				501: 'Сервер отклонил запрос из-за неправильного типа запроса.',
				502: 'Кажется, сервер отключен.',
				503: 'Сервер перегружен или недоступен.',
				504: 'Кажется, сервер отключен.'
			};
			// 401 означает, что мы не авторизованы.
			if (res.status == 401) {
				return reject('<strong>Сессия истекла.</strong><br>Пожалуйста, перезагрузите страницу.');
			} else {
				// Разбираемся индивидуально
				let message = res.status + ' ' + res.statusText + '<br>' +
					(dict[res.status] ? dict[res.status] : 'Ошибка запроса к серверу.') + '<br>' +
					(additionalInfo ? JSON.stringify(additionalInfo) + '<br>' : '');
				// Получили ли мы тело ответа, или только код?
				if (res.body && (!res.headers.has("content-length") || (Number(res.headers.get("content-length"))) > 0)) {
					return res.text()
						.then(data => {
							return reject(message + data);
						})
						.catch(e => {
							return reject(message + e);
						});
				} else {
					reject(message + 'Сервер не вернул дополнительной информации об ошибке.');
				}
			}
		}
	});
};

// Регистрация ивентов
document.addEventListener('DOMContentLoaded', () => {
	errorMsg = document.getElementById("errorMsg");
	errorBody = document.getElementById("errorText");
	if (errorMsg && errorBody) {
		window.addEventListener('error', handleError);
		window.addEventListener('unhandledrejection', handleError);
		// Кнопка закрытия ошибки
		document.getElementsByClassName("error-close")[0].addEventListener('click', function() {
			errorMsg.classList.add("is-hidden");
			errorBody.textContent = '';
		});
	}
});