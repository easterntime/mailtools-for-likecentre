// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

const Body = document.getElementById('mainTextArea');
const ResultArea = document.getElementById('resultArea');
const PreviewArea = document.getElementById('previewArea');
const ImportArea = document.getElementById('importTextArea');
const NotesArea = document.getElementById('notesArea');

const inputBannerLink = document.getElementById('inputBannerLink');
const inputButtonColor = document.getElementsByName('inputButtonColor');
const inputButtonColorCustom = document.getElementById('inputButtonColorCustom');
const inputDarkerLinks = document.getElementById('inputDarkerLinks');
const inputFontColor = document.getElementsByName('inputFontColor');
const inputGif1 = document.getElementById('inputGif1');
const inputGif2 = document.getElementById('inputGif2');
const inputMainLink = document.getElementById('inputMainLink');
const inputHeader = document.getElementById('inputHeader');
const inputPreheader = document.getElementById('inputPreheader');
const inputName = document.getElementById('inputName');

const colorCustomPreview = document.getElementById('colorCustomPreview');

const buttonCopy = document.getElementById('copy');
const buttonDownload = document.getElementById('download');

let bannerLink = '';
let buttonColor = '';
let linkDarker = false;
let linkColor = '';
let fontColor = '#FFF';
let gif1Link = '';
let gif2Link = '';
let mainLink = '';
let header = '';
let preheader = '';
let documentID = '';

//Если true, то мы находимся в тестовом режиме
//и сетевые функции должны быть отключены
let demoMode = true;

// Кидает необработанную ошибку
const testError = function() {
	//let a = ass;
	return new Promise((resolve,reject) => {
		reject(new Error('Unhandled Rejection, yay!'));
	}).catch(e => handleCatch(e));
	// return new Promise((resolve,reject) => {
		// return resolve('Answer1');
	// }).then(data => {
		// return data + ' Answer2';
	// });
};

//Если переменная пустая, ставит значение по умолчанию
const def = function(a,defaultValue) {
	return a == null ? defaultValue : a;
};

const replaceIframeContent = function(iframeElement, newHTML) {
    iframeElement.src = "about:blank";
	//iframeElement.srcdoc = newHTML;
	// iframeElement.contentWindow.location.reload(true);
    iframeElement.contentWindow.document.open();
    iframeElement.contentWindow.document.write(newHTML);
    iframeElement.contentWindow.document.close();
};

// Оборачивает текст инпута в тег
const mark = function(code) {
	if(typeof Body.selectionStart == 'number' && typeof Body.selectionEnd == 'number') {
		let start = Body.selectionStart;
		let end = Body.selectionEnd;
		
		let selectedText = Body.value.slice(start, end);
		let before = Body.value.slice(0, start);
		let after = Body.value.slice(end);
		
		Body.value = before + '[' + code + ']' + selectedText + '[/' + code + ']' + after;
		
		goSubmit();
	}
};

// Заменяет выделенный текст на переданное значение
const markNoBB = function(code) {
	if(typeof Body.selectionStart == 'number' && typeof Body.selectionEnd == 'number') {
		let start = Body.selectionStart;
		let end = Body.selectionEnd;
		
		let before = Body.value.slice(0, start);
		let after = Body.value.slice(end);
		
		Body.value = before + code + after;
		
		goSubmit();
	}
};

// Эта функция санитайзит поля ввода
// Соурс: https://stackoverflow.com/a/48226843/13952888
const sanitize = function(string) {
  const map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',
      "/": '&#x2F;',
  };
  return string.replace(/[^\p{L}\p{N}\p{P}\p{Z}{\^\$}]/gu, '').replace(/[&<>"'/]/ig, (match)=>(map[match]));
};

// Функция срабатывает при изменении полей ввода
const editElementChange = function(eventstuff = undefined) {
	//Достаём значения ссылочек и прочего
	bannerLink = encodeURI(inputBannerLink.value.trim());
	//Цвет кнопок, включая кастомный
	inputButtonColor.forEach((el) => {
		if (el.checked) {
			buttonColor = el.value;
		}
	});
	if (buttonColor == 'custom') {
		//Туду: валидация, а не санитизация
		buttonColor = sanitize(inputButtonColorCustom.value.trim());
		if (buttonColor.search('#')!=0) {
			buttonColor = '#'+buttonColor;
		}
	}
	//Затемнение цвета шрифта ссылок
	if (inputDarkerLinks.checked) {
		linkColor = pSBC(-0.4,buttonColor);
	} else {
		linkColor = buttonColor;
	}
	//Цвет шрифта
	inputFontColor.forEach((el) => {
		if (el.checked) {
			fontColor = el.value;
		}
	});
	gif1Link = encodeURI(inputGif1.value.trim());
	if (gif1Link.search(/media\d{0,3}\.giphy\.com/gmi)>-1) {
		if (gif1Link.search(/^.+?giphy\.gif/gmi)>-1) {
			gif1Link = gif1Link.replace(/^(.+?giphy\.gif).*/gmi,'$1');
		}
	}
	gif2Link = encodeURI(inputGif2.value.trim());
	if (gif2Link.search(/media\d{0,3}\.giphy\.com/gmi)>-1) {
		if (gif2Link.search(/^.+?giphy\.gif/gmi)>-1) {
			gif2Link = gif2Link.replace(/^(.+?giphy\.gif).*/gmi,'$1');
		}
	}
	
	//taskID = encodeURI(inputTaskID.value.trim());
	taskID = '';
	mainLink = encodeURI(inputMainLink.value.trim());
	header = sanitize(inputHeader.value).trim();
	
	if (eventstuff) {
		goSubmit();
	}
};

// Основная функция обработки данных
// complete перезагружает все данные полностью
const goSubmit = function(complete = false) {
	//Копируем телеса
	let lines = Body.value.replace(/\p{Emoji}/ug, (m, idx) => he.encode(m)).split(/\r?\n/);
	
	if (complete) {
		editElementChange();
	}
	
	//Начинаем обработку
	//Флаги контекста
	let list = false;
	//let grey = false;
	
	let max1 = lines.length;
	for(let i=0;i<max1;i++) {
		if (lines[i].includes('[ListA]') || lines[i].includes('[ListB]')) {list = true;}
		
		//Расставим абзацы
		//Игнорируем пустые строки или строки, на которых один только тег
		if (lines[i].trim().replace(/^\[[^\[]*\]$/gi,'').trim() != '') {
			if (!list) {
				//Если это значащий абзац и не список
			if (lines[i].search(/\[(?:Btn|Gif\d|P)(?:=["']|])/i) < 0) {
			// if (!lines[i].includes('[Btn]') && !lines[i].includes('[Gif') && !lines[i].includes('[P]')) {
					lines[i] = PStart + lines[i] + PEnd;
				}
			} else {
				//Итем списка
				lines[i] = ListItemStart + lines[i] + ListItemEnd;
			}
		}
		//Листы и греи всегда должны быть в начале и конце строки, не в середине
		if (lines[i].includes('[ListA]')) {
			lines[i] = lines[i].replace('[ListA]','');
			lines[i] = lines[i] = ListAStart + lines[i];
		}
		if (lines[i].includes('[/ListA]')) {
			list = false;
			lines[i] = lines[i].replace('[/ListA]','');
			lines[i] = lines[i] += ListAEnd;
		}
		if (lines[i].includes('[ListB]')) {
			lines[i] = lines[i].replace('[ListB]','');
			lines[i] = lines[i] = ListBStart + lines[i];
		}
		if (lines[i].includes('[/ListB]')) {
			list = false;
			lines[i] = lines[i].replace('[/ListB]','');
			lines[i] = lines[i] += ListBEnd;
		}
		if (lines[i].includes('[Grey]')) {
			lines[i] = lines[i].replace('[Grey]','');
			lines[i] = GreyStart + lines[i];
		}
		if (lines[i].includes('[/Grey]')) {
			lines[i] = lines[i].replace('[/Grey]','');
			lines[i] += GreyEnd;
		}
	}
	
	//Теперь глобальные замены
	//тут же присоединяется баннер
	let result = Banner() + lines.join("\n");
	
	result = result.replace(/\[P\](.*?)\[\/P\]/gmi,PStart+'$1'+PEnd);
	result = result.replaceAll('[B]',BStart);
	result = result.replaceAll('[/B]',BEnd);
	result = result.replaceAll('[I]',IStart);
	result = result.replaceAll('[/I]',IEnd);
	result = result.replaceAll('[Link]',LinkStart());
	result = result.replace(/\[Link\=\"(.+?)\"\]/gmi,LinkStart('$1'));
	result = result.replaceAll('[/Link]',LinkEnd);
	result = result.replaceAll('[Btn]',BtnStart());
	result = result.replaceAll(/\[Btn\=\"(.+?)\"\]/gmi,BtnStart('$1'));
	result = result.replaceAll('[/Btn]',BtnEnd);
	result = result.replaceAll('[Gif1]',Gif1Start());
	result = result.replaceAll('[/Gif1]',Gif1End);
	result = result.replaceAll('[Gif2]',Gif2Start());
	result = result.replaceAll('[/Gif2]',Gif2End);
	result = result.replaceAll('[nowrap]',nowrapStart);
	result = result.replaceAll('[/nowrap]',nowrapEnd);
	
	// Вторая прогонка, чтобы правильно расставить пробелы
	lines = result.split(/\r?\n/);
	max1 = lines.length;
	for(i=0;i<max1;i++) {
		lines[i] = '                ' + lines[i];
	}
	
	result = lines.join("\n");
	
	//Вывод результата и превью
	ResultArea.value = result;
	
	replaceIframeContent(PreviewArea,previewStart + result + previewEnd);
	
	//PreviewArea.innerHTML = previewStart + result + previewEnd;
};

const goImport = function() {
	let theText = ImportArea.value;
	
	// Удаляем комментарии
	theText = theText.replace(/<!--(.|[\r\n])*?-->/gmi,'');
	// Меняем <br> на переносы строки
	theText = theText.replace(/<\/?br\s?\/?>/gmi,'\n');
	// Удаляем неделимые пробелы
	theText = theText.replaceAll('&nbsp;',' ');
	// Удаляем пробелы в начале и конце строк
	theText = theText.replaceAll(/^[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]+/gmi,'');
	theText = theText.replaceAll(/[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]+$/gmi,'');
	// Удаляем пустые строки
	theText = theText.replace(/^\n\s*/gmi,'');
	// Меняем начало и конец абзаца на теги
	theText = theText.replace(/<p.*?>\w*\n/gmi,'[P]');
	theText = theText.replace(/\n\w*<\/p.*?>/gmi,'[/P]');
	// Ловим осиротевшие [P],[/P]
	theText = theText.replace(/(\[P\].*?)(?<!\[\/P\])$/gmi,'$1[/P]');
	theText = theText.replace(/^(?!\[P\])(.*?\[\/P\])/gmi,'[P]$1');
	// Гифки
	theText = theText.replace(/\[P.*\]\r?\n?.*giphy\.com.*\n?\[\/P\]/m,'[Gif1][/Gif1]');
	theText = theText.replace(/\[P.*\]\r?\n?.*giphy\.com.*\n?\[\/P>\]/m,'[Gif2][/Gif2]');
	// Жирный шрифт
	theText = theText.replace(/<(strong|b)>/gmi,'[B]');
	theText = theText.replace(/<\/(strong|b)>/gmi,'[/B]');
	// Италик
	theText = theText.replace(/<(em|i)>/gmi,'[I]');
	theText = theText.replace(/<\/(em|i)>/gmi,'[/I]');
	// Подчёркивание (ссылка)
	theText = theText.replace(/<span.*?>\s*/gmi,'[Link]');
	theText = theText.replace(/<\/span.*?>\s*/gmi,'[/Link]');
	// Пункты списка (удаляются)
	theText = theText.replace(/<\/?li>/gmi,'');
	// Сам список (дефолтится в А)
	theText = theText.replace(/<ul.*?>\w*/gmi,'[ListA]');
	theText = theText.replace(/<\/ul.*?>\w*/gmi,'[/ListA]');
	theText = theText.replace(/<ol.*?>\w*/gmi,'[ListB]');
	theText = theText.replace(/<\/ol.*?>\w*/gmi,'[/ListB]');
	// Если список у нас из чёрточек
	theText = theText.replace(/(^-\s?(.|[\r\n])*?)\n(?!-)/gmi,'[ListA]\n$1\n[/ListA]\n');
	theText = theText.replace(/^-\s?.*?/gmi,'');
	// Кнопки
	theText = theText.replace(/\[P\]\*(.*)\*.*\[\/P\]/gmi,'[Btn]$1[/Btn]');
	// Удаляем лишние ссылки
	theText = theText.replace(/\[P\]<a.*<\/a>\[\/P\]\n/gmi,'');
	// Пробелы перед тегами - в топку
	theText = theText.replace(/(?=.*) (?=\[\/|$)/gmi,'');
	// И добавить после тегов там, где это кошерно
	theText = theText.replace(/(\[\/([IB]|Link)\])(?!\[|\s)/gmi,'$1 ');
	// Выдираем заголовок
	if (theText.search(/\[P\](Заголовок|Тема):\s.*?\[\/P\]/m)>-1) {
		try {
			inputHeader.value = theText.match(/\[P\](Заголовок|Тема):\s.*?\[\/P\]/m)[0].replace(/\[P\](Заголовок|Тема):\s(.*?)\[\/P\]/m,'$2');
			inputPreheader.value = theText.match(/\[P\](Прех|Прехедер|Подзаголовок):\s.*?\[\/P\]/m)[0].replace(/\[P\](Прех|Прехедер|Подзаголовок):\s(.*?)\[\/P\]/m,'$2');
			sendCustom({
				'header': inputHeader.value,
				'preheader': inputPreheader.value
			});
		} catch(e) {
			console.log(e, 'Опять заголовки не поймались...');
		}
	}
	theText = theText.replace(/\[P\](Заголовок|Тема|Прех|Прехедер|Подзаголовок):\s.*?\[\/P\]/gmi,'');
	// Красивые переносики
	theText = theText.replace(/(?!^)\n(?!\s*\n)/gmi,'\n\n');
	// Бонус: неделимые пробелы перед смайликами и между числами
	// theText = theText.replace(/\s(:\)|;\))/gmi,'&nbsp;$1');
	// theText = theText.replace(/(\d+)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]{1}(\d+)/gmi,'$1&nbsp;$2');
	// theText = theText.replace(/(\d+|млн\.?|тыс\.?)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000](млн\.?|тыс\.?|р\.|руб\.?|₽)/gmi,'$1&nbsp;$2')
	
	Body.value = theText;
	goSubmit();
};

// Обрабатывает текст, чтобы он был готов к публикации в емейле
const makeGood = function() {
	let theText = Body.value;
	
	//Номер задачи: 294619
	
	emojiRegex = /\p{Emoji}/ug;
	// /([^\p{L}\p{N}\p{P}\p{Z}{\^\$}])/gu;
	// /\p{Emoji}/ug;
	
	// 1. Все эмодзи конвертируются в HTML entity.
	header = inputHeader.value = inputHeader.value.replace(emojiRegex, (m, idx) => he.encode(m));
	preheader = inputPreheader.value = inputPreheader.value.replace(emojiRegex, (m, idx) => he.encode(m));
	theText = theText.replace(emojiRegex, (m, idx) => he.encode(m))
	
	// 14. Несколько простых пробелов подряд сливаются в один.
	.replace(/[ ]{2,}/gmi,' ')
	
	// 2. Перед смайликами и эмодзи ставятся неделимые пробелы.
	// Не могу сделать пробелы перед эмодзи. Не знаю, как это сделать, чтобы ничего не поломать.
	.replace(/\s?([:;][)(|/\\*])/gmi,'&nbsp;$1')
	
	// 3. Между каждым числом и следующим числом или словом ставится неделимый пробел.
	.replace(/(\d+)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000](?!%)/gmi,'$1&nbsp;')
	
	//Исключение: перед знаком процента '%' ставится короткий неделимый пробел.
	.replace(/(\d+)[\t\v\f\r \u00a0\u2000-\u200b\u2028-\u2029\u3000]?(?=%)/gmi,'$1&#8239;')
	
	// 4. Минусы "-", отбитые двумя пробелами или только пробелом справа, становятся тире, отбитым пробелами с двух сторон. Левый - неделимый.
	// 5. Длинное тире между двумя словами отбивается неделимым пробелом с левой стороны и обычным - справа.
	.replace(/(?:&nbsp;|\s?)(?:-|—)\s/gmi, '&nbsp;— ')
	
	// 6. Минус или тире между двумя числами, не отбитый пробелами, превращается в короткое тире.
	// Короткое тире разрывается, а разрывать нельзя. Вот ведь зараза.
	// Сначала заменяем на числовое тире
	.replace(/(?<=\d+)[-–—‐](?=\d+)/gmi, '&#8210;')
	// Затем объединяем всё число в неразрывную группу
	.replace(/(?<=^|\s)((?:\d)+(?:&#8210;)+\S+)/gmi, '[nowrap]$1[/nowrap]')
	
	// 7. Минус или дефис между двумя словами, не отбитый пробелами, превращается в неделимый дефис.
	.replace(/(?<=\w|[А-я])[-‐](?=\S)/gmi, '&#8209;')
	
	// 8. Все слова длиной 1 или 2 буквы приклеиваются неделимым пробелом к следующему слову. Исключение: "ли".
	.replace(/((?<=^|\s|&nbsp;)(?!ли)[а-я*]{1,2})(?!$)\s/gmi, '$1&nbsp;')
	
	// 9. Все предлоги, с которых начинается предложение, приклеиваются к следующему слову.
	.replace(/(?<=Без|Безо|Близ|В|Во|Вместо|Вне|Для|До|За|Из|Изо|Из-за|Из-под|К|Ко|Кроме|Между|Меж|На|Над|Надо|О|Об|Обо|От|Ото|Перед|Передо|Пред|Предо|Пo|Под|Подо|При|Про|Ради|С|Со|Сквозь|Среди|У|Через|Чрез)\s/gm, '&nbsp;')
	
	// 10. "Ли" и "себя" приклеиваются вместо этого к предыдущему слову, если они не первые в предложении.
	.replace(/(?<!^)\s(?=(?:себя|ли)[^А-яA-z])/gmi,'&nbsp;')
	
	// 13. Три точки подряд превращаются в символ троеточия.
	.replace(/\.{3}/gmi,'&#8230;')
	
	// 15. Убираются пробелы перед знаками препинания.
	.replace(/\s([.,!?;:])/gmi, '$1')
	
	// 11. Длинные числа, отбитые с левой стороны пробелом, разбиваются по три знака справа налево. Единица измерения отбивается неделимым пробелом. Пример: ' 1000000₽' -> ' 1 000 000 ₽'.
	.replace(/\d+(?=руб|р\.|₽|тыс|млн|миллионов|млрд|миллиардов)/gmi, '$&&nbsp;')
	// Экспериментально!
	.replace(/(?<=\s|&nbsp;|^)\d{4,}(?=\s|&nbsp;|$)/gmi, (sMatch) => {
		let result = '';
		let len = sMatch.length;
		const high = Math.floor((len-1)/3)-1;
		for (let i=0; i <= high; i++) {
			result = '&nbsp;' + sMatch.slice(len-(i*3)-3, len-(i*3)) + result;
		}
		return sMatch.slice(0,len-(high+1)*3) + result;
	})
	
	// 12. Программистские кавычки " ... " меняются на французские ёлочки « ... ».  Так и не придумал, как быстро реализовать вложенные кавычки.
	.replace(/"(?=[А-яA-z])/gmi, '«')
	.replace(/(?<=[А-яA-z])"/gmi, '»');
	
	// Другие части речи - неделимый пробел ПОСЛЕ
	// (я|мы|ты|вы|он|она|оно|они|мой|твой|ваш|наш|свой|его|ее|её|их|весь|кто|что|какой|каков|чей|сколько|никто|ничто|некого|нечего|никакой|ничей|нисколько||||||||||||||||||||||||||||||||)
	
	Body.value = theText;
	goSubmit();
};

// Эта функция копирует в буфер обмена содержимое textfield'а "Результат"
const goCopy = function() {
	// Добавляем анимацию загрузки
	buttonCopy.classList.add("is-loading");
	
	// Отправляем текст на копирование
	let data = ResultArea.value;
	navigator.clipboard.writeText(data).then(()=>{
		//Убираем анимацию загрузки
		buttonCopy.classList.remove("is-loading");
		//Добавляем галочку
		buttonCopy.innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Скопировано!</span>';
		//Возвращаем кнопку в нормальное состояние через 2 секунды
		setTimeout(() => {
			buttonCopy.innerHTML = 'Скопировать результат';
		}, 2000);
	}).catch(e => handleCatch(e));
};

// Эта функция скачивает весь проект как файл
const goDownload = function() {	
	// Отправляем текст на копирование
	let data = {};
	data.importText = importTextArea.value;
	data.mainText = Body.value;
	data.bannerLink = bannerLink;
	data.buttonColor = buttonColor;
	data.linkColor = linkColor;
	data.fontColor = fontColor;
	data.gif1Link = gif1Link;
	data.gif2Link = gif2Link;
	data.mainLink = mainLink;
	data.header = header;
	data.preheader = preheader;
	
	download(JSON.stringify(data, null, 2),'export.json');
};

// Эта функция ставит дефолтные настройки цвета шрифта ссылок
// при выборе цвета кнопок
const colorChange = function(fontColor,darkerLinks) {
	inputDarkerLinks.checked = darkerLinks;
	
	inputFontColor.forEach((el) => {
		if (el.value == fontColor) {
			el.checked = true;
		}
	});
	
	linkDarker = darkerLinks;
	fontColor = fontColor;
	
	sendCustom({
		'linkDarker': linkDarker,
		'fontColor': fontColor
	});
};

// Эта функция ограничивает частоту срабатывания автоматического предпросмотра
// https://towardsdev.com/debouncing-and-throttling-in-javascript-8862efe2b563
const throttle = function(func, limit) {
  let lastFunc;
  let lastRan;
  return function() {
    const context = this;
    const args = Array.prototype.slice.call(arguments,2);
    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function() {
          if ((Date.now() - lastRan) >= limit) {
            func.apply(context, args);
            lastRan = Date.now();
          }
       }, limit - (Date.now() - lastRan));
    }
  };
};

// Эта функция позволяет скачать текст как файл
// Соурс: https://stackoverflow.com/a/34156339/13952888
const download = function (content, fileName, contentType = 'text/plain') {
    const a = document.createElement("a");
    let file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
};

//Загружает документ с сервера
const refresh = function() {
	//Превью цвета
	colorCustomPreview.style.color = inputButtonColorCustom.value.charAt(0) === '#' ? inputButtonColorCustom.value : '#'+inputButtonColorCustom.value;
	//Получаем айди
	const urlParams = new URLSearchParams(window.location.search);
	if (!urlParams.has('id')) {
		//Если id нет, то мы открыли редактор в тестовом режиме.
		demoMode = true;
		return;
	}
	documentID = urlParams.get('id');
	fetchGet('/tools/list/document.json', {'id': documentID})
		.then(res => {
			if (res.status == 404) {
				//Если нет такого файла, уходим в demoMode
				history.pushState({}, '', window.location.origin + window.location.pathname);
				demoMode = true;
				throw new Error('Файл с таким ID не найден.');
			}
			// Хэндлинг ошибок от сервера
			return handleCodes(res);
		})
		.then(res => res.json())
		.then(data => {
			document.getElementById('breadcrumbName').innerText = data.name;
			inputName.value = data.name;
			demoMode = false;
			ImportArea.value = data.importText;
			Body.value = data.mainText;
			NotesArea.value = data.notesText;
			bannerLink = data.bannerLink;
			inputBannerLink.value = data.bannerLink;
			buttonColor = data.buttonColor;
			let succ = false;
			//Совпадение со списком цветов
			inputButtonColor.forEach((el) => {
				if (succ) {return;}
				if (el.value == buttonColor) {
					el.checked = true;
					succ = true;
				} else {
					el.checked = false;
				}
			});
			if (!succ) {
				//Кастомное значение
				inputButtonColor[inputButtonColor.length-1].checked = true;
				inputButtonColorCustom.value = data.buttonColor;
			}
			//Превью цвета
			colorCustomPreview.style.color = data.buttonColor.charAt(0) === '#' ? data.buttonColor : '#'+data.buttonColor;
			//Остальное
			linkDarker = data.linkDarker;
			inputDarkerLinks.checked = data.linkDarker;
			fontColor = data.fontColor;
			inputFontColor.forEach((el) => {
				el.checked = (el.value == data.fontColor);
			});
			gif1Link = data.gif1Link;
			inputGif1.value = data.gif1Link;
			gif2Link = data.gif2Link;
			inputGif2.value = data.gif2Link;
			mainLink = data.mainLink;
			inputMainLink.value = data.mainLink;
			header = data.header;
			inputHeader.value = data.header;
			preheader = data.preheader;
			inputPreheader.value = data.preheader;
		})
		.catch(e => {
			if (e instanceof Error) {
				handleCatch(e);
			} else {
				handleCatch(new Error(JSON.stringify(e)));
			}
		});
};

// Отправляет изменения в реалтайме на сервер
// Вероятно будет переписано позже на websockets
const sendChanges = function(context) {
	if (demoMode) {return;}
	const dict = {
		'importTextArea': 'importText',
		'mainTextArea': 'mainText',
		'inputBannerLink': 'bannerLink',
		'inputGif1': 'gif1Link',
		'inputGif2': 'gif2Link',
		'inputMainLink': 'mainLink',
		'inputHeader': 'header',
		'inputPreheader': 'preheader',
		'inputDarkerLinks': 'linkDarker'
	};
	let params = {};
	params.id = documentID;
	if (this.type === 'checkbox') {
		params[dict[this.id]] = this.checked;
	} else {
		params[dict[this.id]] = this.value;
	}
	fetchUpdate("/tools/list/document.json", params)
		.then(res => handleCodes(res))
		.catch(e => {
			// Если это уже ошибка, нет смысла создавать новую ошибку
			if (e instanceof Error) {
				handleCatch(e);
			} else {
				handleCatch(new Error(JSON.stringify(e)));
			}
		});
};

// То же самое, но для радио
// Я не уверен, что мы можем доверять клику для определения значения
const sendChangesRadio = function(context) {
	if (demoMode) {return;}
	let radioSet = document.getElementsByName(this.name);
	
	const dict = {
		'inputButtonColor': 'buttonColor',
		'inputFontColor': 'fontColor'
	};
	let params = {};
	
	for (let el of radioSet) {
		if (el.checked) {
			// Если это "Свой" цвет
			if (el.value == 'custom') {
				params[dict[this.name]] = inputButtonColorCustom.value;
			} else {
				params[dict[this.name]] = el.value;
			}
			break;
		}
	}
	
	params.id = documentID;
	params[dict[this.id]] = this.value;
	fetchUpdate("/tools/list/document.json", params)
		.then(res => handleCodes(res))
		.catch(e => {
			// Если это уже ошибка, нет смысла создавать новую ошибку
			if (e instanceof Error) {
				handleCatch(e);
			} else {
				handleCatch(new Error(JSON.stringify(e)));
			}
		});
};

// Отправка программных изменений, не по клику
const sendCustom = function(params) {
	if (demoMode) {return;}
	params.id = documentID;
	fetchUpdate("/tools/list/document.json", params)
		.then(res => handleCodes(res))
		.catch(e => {
			// Если это уже ошибка, нет смысла создавать новую ошибку
			if (e instanceof Error) {
				handleCatch(e);
			} else {
				handleCatch(new Error(JSON.stringify(e)));
			}
		});
};

// Открывает модальную форму создания UTM-ссылок
const wrapOpen = function() {
	// Открываем форму
	document.getElementById('modalWrap').classList.add('is-active');
	// Парсим имеющуюся ссылку
	const url = new URL (inputMainLink.value);
	// Основная ссылка, без квери
	document.getElementById('modalWrapLink').value = url.origin + url.pathname;
	// Номер задачи
	if (url.searchParams.has('utm_content')) {
		document.getElementById('modalWrapTask').value = url.searchParams.get('utm_content');
	} else {
		// Пробуем предположить - выбираем из задачи подходящий номер
		let match = inputName.value.match(/\d{5,8}/);
		if (match) {
			document.getElementById('modalWrapTask').value = match;
		}
	}
	// Тип рассылки
	if (url.searchParams.has('utm_term')) {
		document.getElementById('modalWrapType').value = url.searchParams.get('utm_term');
	} else {
		// Пробуем предположить - выбираем из задачи подходящий тайтл
		let matched = false;
		let name = inputName.value;
		let dict = {
			'annonce': 'annonce_kp',
			'service': 'service_kp',
			'native': 'native_kp',
			'emergency': 'emergency',
			'auto': 'autoemail'
		};
		for (const [key, value] of Object.entries(dict)) {
			if (name.includes(key)) {
				matched = true;
				document.getElementById('modalWrapType').value = value;
				break;
			}
		}
		if (!matched) {
			document.getElementById('modalWrapType').value = 'annonce_kp';
		}
	}
	// Продукт
	if (url.searchParams.has('utm_product')) {
		document.getElementById('modalWrapProduct').value = url.searchParams.get('utm_product');
	} else {
		// Пробуем предположить - выбираем из задачи подходящий продукт
		let matched = false;
		let name = inputName.value;
		['concentrat','reality','speed','sotka','strategy','msa','event'].forEach(el => {
			if (name.includes(el)) {
				matched = true;
				document.getElementById('modalWrapProduct').value = el;
			}
		});
	}
	
	wrapChange();
};

// Оборачивает ссылку в UTM-метки и выводит в превью
const wrapChange = function() {
	let origin = document.getElementById('modalWrapLink').value.trim();
	let url = new URL (origin);
	url.searchParams.set('utm_campaign', origin.substr(origin.lastIndexOf('/')+1));
	url.searchParams.set('utm_medium', 'email');
	url.searchParams.set('utm_source', 'emarsys');
	url.searchParams.set('utm_content', document.getElementById('modalWrapTask').value.trim());
	url.searchParams.set('utm_term', document.getElementById('modalWrapType').selectedOptions[0].value.trim());
	url.searchParams.set('utm_product', document.getElementById('modalWrapProduct').selectedOptions[0].value.trim());
	
	document.getElementById('modalWrapPreview').value = url.toString();
	//https://mestum.com/events/sotka6?utm_campaign=sotka6&utm_medium=email&utm_source=emarsys&utm_content=email_1610_1900&utm_term=service_kp&utm_product=sotka
};

// Ставит ссылку из превью в основную ссылку
const wrapAccept = function() {
	mainLink = document.getElementById('modalWrapPreview').value;
	inputMainLink.value = mainLink;
	this.closest('.modal').classList.remove('is-active');
	sendCustom({'mainLink': mainLink});
};

// Создаёт объект для отправки
const dataForSave = function(parent = null, id = null) {
	let result = {};
	if (id) {
		result.id = id;
	}
	if (parent) {
		result.parent = parent;
	}
	result.name = inputName.value.trim();
	result.mainText = Body.value.trim();
	result.notesText = NotesArea.value.trim();
	result.bannerLink = bannerLink = inputBannerLink.value.trim();
	//Цвет кнопок, включая кастомный
	inputButtonColor.forEach((el) => {
		if (el.checked) {
			buttonColor = el.value;
		}
	});
	if (buttonColor == 'custom') {
		//Туду: валидация, а не санитизация
		buttonColor = sanitize(inputButtonColorCustom.value.trim());
		if (buttonColor.search('#')!=0) {
			buttonColor = '#'+buttonColor;
		}
	}
	result.buttonColor = buttonColor;
	result.linkDarker = linkDarker = inputDarkerLinks.checked;
	inputFontColor.forEach((el) => {
		if (el.checked) {
			fontColor = el.value;
		}
	});
	result.fontColor = fontColor;
	result.gif1Link = gif1Link = inputGif1.value.trim();
	result.gif2Link = gif2Link = inputGif2.value.trim();
	result.mainLink = mainLink = inputMainLink.value.trim();
	result.header = header = inputHeader.value.trim();
	result.preheader = preheader = inputPreheader.value.trim();
	return result;
};

let treeview;

// Открывает модальную форму сохранения файла
// Если он уже сохранён, то делаем ручной полный сейв
const saveOpen = function() {
	//Рекурсивная функция создания дерева
	const setChildren = function(node, source) {
		source.children.forEach(el => {
			let newchild = new TreeNode(el.name);
			newchild.changeOption('id', el._id);
			node.addChild(newchild);
			setChildren(newchild, el);
		});
	};
	
	//Если имя пустое, выходим
	if (!inputName.value.trim()) {
		return;
	}
	
	if (demoMode) {
		fetchGet('/tools/list/directorylist.json', {})
			.then(res => handleCodes(res))
			.then(res => res.json())
			.then(data => {
				let root = new TreeNode('root', {'id': '/'});
				if (treeview) {
					//Очистка всех нод
					// root = treeview.getRoot();
					// root.getChildren().map(el => {
						// root.removeChild(el);
					// })
					treeview.setRoot(root);
				} else {
					//Создание нового дерева
					treeview = new TreeView(root, document.getElementById('treeview'));
					treeview.changeOption("leaf_icon", '<i class="fas fa-folder"></i>');
					treeview.changeOption("parent_icon", '<i class="fas fa-folder"></i>');
				}
				//Формируем дерево
				setChildren(root,data);
				root.setSelected(true);
				treeview.reload();
				document.getElementById('modalSave').classList.add('is-active');
			})
			.catch(e => {
				// Если это уже ошибка, нет смысла создавать новую ошибку
				if (e instanceof Error) {
					handleCatch(e);
				} else {
					handleCatch(new Error(JSON.stringify(e)));
				}
			});
	} else {
		// Добавляем анимацию загрузки
		let saveButton = document.getElementById('saveButton');
		saveButton.classList.add("is-loading");
		// Постим запрос
		fetchUpdate('/tools/list/document.json', dataForSave(null, documentID))
			.then((res) => {
				//Убираем анимацию загрузки
				saveButton.classList.remove("is-loading");
				if (res.status == 200) {
					//Добавляем галочку
					saveButton.innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Сохранено!</span>';
				} else {
					//Добавляем крестик
					saveButton.innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка! Код: '+res.status+'</span>';
					console.log(res.body);
				}
				//Возвращаем кнопку в нормальное состояние через 2 секунды
				setTimeout(() => {
					saveButton.innerHTML = 'Сохранить';
				}, 2000);
			})
			.catch(e => {
				//Убираем анимацию загрузки
				saveButton.classList.remove("is-loading");
				//Добавляем крестик
				saveButton.innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка!</span>';
				handleCatch(e);
				//Возвращаем кнопку в нормальное состояние через 2 секунды
				setTimeout(() => {
					saveButton.innerHTML = 'Сохранить';
				}, 2000);
			});
	}
};

// Сохранение нового документа
const saveAccept = function() {
	fetchPost('/tools/list/document.json', dataForSave(treeview.getSelectedNodes()[0].getOptions().id, null))
		.then(res => handleCodes(res))
		.then(res => res.json())
		.then(data => {
			if (!data.insertedId) {
				console.log(data);
				throw new Error('Поле не было вставлено.');
			}
			const urlParams = new URLSearchParams(window.location.search);
			urlParams.set('id',data.insertedId);
			history.pushState({'id':data.insertedId}, '', window.location.origin + window.location.pathname + '?' + urlParams.toString());
			demoMode = false;
			documentID = data.insertedId;
			document.getElementById('modalSave').classList.remove('is-active');
		})
		.catch(e => {
			// Если это уже ошибка, нет смысла создавать новую ошибку
			if (e instanceof Error) {
				handleCatch(e);
			} else {
				handleCatch(new Error(JSON.stringify(e)));
			}
		});
};

// Открывает диалог создания новой папки
const newDirOpen = function() {
	document.getElementById('modalNewDirName').value = '';
	document.getElementById('modalNewDir').classList.add('is-active');
};

// Создаёт новую папку
const newDirAccept = function() {
	aParent = treeview.getSelectedNodes()[0];
	name = document.getElementById('modalNewDirName').value;
	//aParentID = aParent.getOptions().id;
	fetchPost('/tools/list/directory.json', {
			'name': name,
			'parent': aParent.getOptions().id
		})
		.then(res => handleCodes(res))
		.then(res => res.json())
		.then(data => {
			let newChild = new TreeNode(name, {'id':data.insertedId});
			aParent.addChild(newChild);
			aParent.setSelected(false);
			newChild.setSelected(true);
			treeview.reload();
			document.getElementById('modalNewDir').classList.remove('is-active');
		})
		.catch(e => {
			// Если это уже ошибка, нет смысла создавать новую ошибку
			if (e instanceof Error) {
				handleCatch(e);
			} else {
				handleCatch(new Error(JSON.stringify(e)));
			}
		});
};

let ready = function() {
	//Подцепим листенер к аккордеону
	// bulmaCollapsible.attach('.is-collapsible',{allowMultiple: true});
	
	// Временный фикс айфрейма под Firefox
	replaceIframeContent(PreviewArea,"");
	
	// Кнопочки
	document.getElementById("makeGood").addEventListener('click', makeGood, false);
	
	//document.getElementById('collapsible-message-accordion-meta-1').element.style.height = this.element.scrollHeight + 'px'
	
	// Добавляем листенеры к элементам страницы
	const editElement = document.getElementsByClassName('mainEditElement');
	
	Body.addEventListener('input', throttle(goSubmit,2000), false);
	Body.addEventListener('input', throttle(sendChanges,5000), false);
	notesArea.addEventListener('input', throttle(sendChanges,5000), false);
	for (let el of editElement) {
		if (el.type == 'radio') {
			el.addEventListener('click', editElementChange, false);
			el.addEventListener('click', throttle(sendChangesRadio,5000), false);
		} else {
			el.addEventListener('change', editElementChange, false);
			el.addEventListener('change', sendChanges, false);
		}
	}
	
	//Автоматически отмечаем "Свой" цвет, если мы изменили свой цвет
	inputButtonColorCustom.addEventListener('change', function() {
		inputButtonColor[inputButtonColor.length-1].checked = true;
		colorCustomPreview.style.color = this.value.charAt(0) === '#' ? this.value : '#'+this.value;
		sendCustom({'buttonColor': this.value});
	}, false);
	
	//Кнопка закрытия форм
	let temp = document.getElementsByClassName('modalClose');
	for (let el of temp) {
		el.addEventListener('click', function() {
			this.closest('.modal').classList.remove('is-active');
		}, false);
	}
	
	// UTM-конструктор
	document.getElementById('linkWrapButton').addEventListener('click', wrapOpen, false);
	temp = document.getElementsByClassName('modalWrapInput');
	for (let el of temp) {
		el.addEventListener('change', wrapChange, false);
	}
	document.getElementById('modalWrapAccept').addEventListener('click', wrapAccept, false);
	
	// Выбор папки для сохранения
	document.getElementById('saveButton').addEventListener('click', saveOpen, false);
	document.getElementById('modalSaveAccept').addEventListener('click', saveAccept, false);
	document.getElementById('modalSaveNewDir').addEventListener('click', newDirOpen, false);
	document.getElementById('newDirAccept').addEventListener('click', newDirAccept, false);
	
	refresh();
};

document.addEventListener("DOMContentLoaded", ready);
// document.addEventListener("load", ready);


// Навбар для мобильных устройств
document.addEventListener('DOMContentLoaded', () => {

  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});

const PStart = '<p style="text-align: justify; margin: 28px">\n   ';
const PEnd = '\n</p>';
const BStart = '<b>';
const BEnd = '</b>';
const IStart = '<i>';
const IEnd = '</i>';
const ListAStart = '<ul style="margin:28px; text-align: justify;">\n';
const ListAEnd = '\n</ul>';
const ListBStart = '<ol style="margin:28px; text-align: justify; list-style-type: decimal;">\n';
const ListBEnd = '\n</ol>';
const ListItemStart = '   <li>\n      ';
const ListItemEnd = '\n   </li>';
const LinkStart = function(aLink = '') {
	if (aLink == '') {aLink = mainLink;}
	return('<b><a href="' + aLink + '" style="color: ' + linkColor + ' !important; text-decoration: none !important; border-bottom: 1px dashed ' + linkColor + ' !important;" target="_blank">');
};
const LinkEnd = '</a></b>';
const BtnStart = function(aLink = '') {
	if (aLink == '') {aLink = mainLink;}
	return('<table bgcolor="' + buttonColor + '" border="0" cellpadding="0" cellspacing="0" height="44" style="border-radius: 6px; margin: 28px 0;" width="auto">\n   <tr>\n      <td class="btn" align="center" height="44" style="min-width:230px; max-width:90vw;" valign="middle"><a href="' + aLink + '" style="color:' + fontColor + ' !important; display: inline-block; text-decoration: none !important; line-height: 44px; font-weight: 700; padding-left: 30px; padding-right: 30px; font-size:18px;" target="_blank">');
};
const BtnEnd = '</a>\n      </td>\n   </tr>\n</table>';
const Gif1Start = function() {
	return('<a href="' + mainLink + '" target="_blank">\n   <img alt="Альт" src="' + gif1Link + '" style="display:block; width:100%; max-width: 300px; height:auto; margin: 28px 0 ;" />\n</a>');
};
const Gif1End = '';
const Gif2Start = function() {
	return('<a href="' + mainLink + '" target="_blank">\n   <img alt="Альт" src="' + gif2Link + '" style="display:block; width:100%; max-width: 300px; height:auto; margin: 28px 0 ;" />\n</a>');
};
const Gif2End = '';

const Banner = function() {
	return('<a href="' + mainLink + '" target="_blank">\n   <img alt="' + header + '" src="' + bannerLink + '" style="display:block; width:100%; height:auto; margin: 28px 0;" />\n</a>\n\n');
};

const GreyStart = '<table bgcolor="#dbdbdb" border="0" cellpadding="0" cellspacing="0" class="container" width="100%" style="margin: 28px 0;">\n                   <tr>\n                      <td align="center" valign="top">';
const GreyEnd = '\n                      </td>\n                   </tr>\n                </table>';

const nowrapStart = '<span style="white-space:nowrap;">';
const nowrapEnd = '</span>';

const previewStart = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="ru"> <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="x-apple-disable-message-reformatting"/> <title></title> <style type="text/css"> @import url(\'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i,900,900i\'); body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}img{-ms-interpolation-mode: bicubic;} img{border: 0; outline: none; text-decoration: none;}table{border-collapse: collapse !important;}body{margin: 0 !important; padding: 0 !important; width: 100% !important;} a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;} div[style*="margin: 16px 0;"]{margin: 0 !important;} @media all and (max-width:699px){.wrapper{width: 100% !important; padding: 0 !important;}.container{width: 100% !important; padding: 0 !important; border-radius: 0 !important;}.container95{width: 95% !important; padding: 0 !important;}.mobile{width: 100% !important; display:block!important; padding: 0 !important;}.img{width:100% !important; height:auto !important;}.die{display: none;}.to-center{text-align: center !important;}.fix1{display: table !important; border-left: 0 !important; border-right: 0 !important; border-bottom: 2px solid #dddddd !important;}p, li{text-align: left !important;}*[class="mobileOff"]{width: 0px !important; display: none !important;}*[class*="mobileOn"]{display: block !important; max-height:none !important;}}@media all and (max-width:475px) {.btn{min-width: 75vw;}}</style> </head> <body style="margin:0; padding:0; background-color:#eaeaea; "><table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:url(\'\') #eaeaea center top no-repeat; font-family: \'Roboto\', Verdana; font-weight: 400; font-size: 16px; color: #111; line-height: 24px;"> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" width="600" class="container fix1"> <tr> <td align="right" style="padding: 10px;" width="50%"> <p style=" margin:0px; font-size: 14px; text-align: right; line-height:14px;"><a href="#HTML_BROWSE_HREF#" style="color: #777777 !important; text-decoration: none !important; border-bottom: none !important;" target="_blank">Веб-версия</a></p></td></tr></table> <table width="600" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container fix1" style="border-left:2px solid #dddddd; border-right:2px solid #dddddd; border-bottom:3px solid #dddddd; border-radius: 12px; display: block;" > <tr> <td align="center" valign="top" width="100%"> <table border="0" cellpadding="0" cellspacing="0" width="auto"> <tr> <td style="padding: 15px 0;" valign="top"><a href="https://likecentre.ru/?utm_medium=email&utm_source=emarsys&utm_campaign=all" target="_blank"><img alt="LIKE Центр" src="http://suite34.emarsys.net/custloads/794454674/md_166407.png" style="display:block; width:auto; height:auto;"/></a></td></tr></table> <!-- Начало контента --> ';
const previewEnd = ' </td></tr><tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" class="wrapper" style="background: url(\'http://suite34.emarsys.net/custloads/794454674/md_166413.png\') right bottom no-repeat; background-color: #36383e; border-radius: 0 0 10px 10px;" width="100%"> <tr> <td align="center" valign="top"> <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container"> <tr> <td width="50%" class="mobile to-center" align="left" valign="top"> <p style="text-align: justify; margin: 28px 28px 14px; font-size: 18px; font-weight: 700; color: #fff; text-align: left"> C уважением,<br/> команда Like Центра </p><p style="margin:4px 28px 4px;"><a href="http://tel:+78005507619" style="color:#ffffff !important; text-decoration: none !important; font-size: 18px; font-weight:500;">8-800-550-76-19</a></p><p style="margin:4px 28px 4px;"><a href="https://likecentre.ru/" style="color:#ffffff !important; text-decoration: none !important; font-size:18px; font-weight:500;">likecentre.ru</a></p><p style="margin:4px 28px 14px; "><a href="http://support1@likebz.ru" style="color:#ffffff !important; text-decoration: none !important; font-size:18px; font-weight:500;">support1@likebz.ru</a></p><p style="margin:14px 28px 14px;"> <a href="https://likecentre.ru/publications" style="color: #ffffff !important; text-decoration: underline !important; font-size: 18px; font-weight:500;">СМИ о нас</a> </p></td><td width="50%" class="mobile to-center" valign="top"> <p style="margin:28px 28px 14px;"> <a href="https://vk.cc/7MUgZS" style="color: #ffffff !important; text-decoration: underline !important; font-size: 18px; font-weight:500;">Like Центр в твоем городе</a> </p><p style="margin:14px 28px 5px; font-size: 16px; font-weight: 700; color: #ffffff; text-align: left; line-height:14px;">Следи за новостями:</p><table border="0" cellpadding="0" cellspacing="0" style="margin: 5px 18px 14px;"> <tr> <td><a href="https://vk.com/likecentre" style="ver" target="_blank"><img alt="Vk" src="http://suite34.emarsys.net/custloads/794454674/md_166409.png" style="display: inline-block; margin: 10px;"/></a></td><td><a href="https://facebook.com/likefamily1/" target="_blank"><img alt="Facebook" src="http://suite34.emarsys.net/custloads/794454674/md_166411.png" style="display: inline-block; margin: 10px;"/></a></td><td><a href="https://www.instagram.com/likecentre.ru/" target="_blank"><img alt="Instagram" src="http://suite34.emarsys.net/custloads/794454674/md_166412.png" style="display: inline-block; margin: 10px;"/></a></td><td><a href="https://www.youtube.com/channel/UCLJEzGSxaSiTpPLWKwPZerw?sub_confirmation=1" target="_blank"><img alt="YouTube" src="http://suite34.emarsys.net/custloads/794454674/md_166410.png" style="display: inline-block; margin: 10px;"/></a></td></tr></table> </td></tr></table> </td></tr><tr> <td height="10" style="font-size:10px; line-height:10px;"></td></tr></table> </td></tr></table> <table width="600" cellpadding="0" cellspacing="0" border="0" class="container95" style="margin: 35px"> <tr> <td align="center" valign="top"> <span style="display:block; font-weight: 300; margin:0; font-size: 12px; color:#a4a4a4; line-height:19px;"> Вы получили это письмо так как подписались на рассылку от Like Центр.<br class="die"/> Подписка на рассылку гарантирует получение доступа к платным продуктам компании. <br class="die"/> Если вы не хотите получать наши письма, то просто <a href="#UNSUBSCRIBE_HREF#" ems:notrack="true" ems:name="unsub" style="color:#909090;">отпишитесь от рассылки</a>.</span> </td></tr></table> </td></tr></table> </body></html>';


// Соурс: https://stackoverflow.com/a/13542669
// Функция преобразования цвета
// Version 4.0
const pSBC=(p,c0,c1,l)=>{
	let r,g,b,P,f,t,h,i=parseInt,m=Math.round,a=typeof(c1)=="string";
	if(typeof(p)!="number"||p<-1||p>1||typeof(c0)!="string"||(c0[0]!='r'&&c0[0]!='#')||(c1&&!a))return null;
	if(!this.pSBCr)this.pSBCr=(d)=>{
		let n=d.length,x={};
		if(n>9){
			[r,g,b,a]=d=d.split(","),n=d.length;
			if(n<3||n>4)return null;
			x.r=i(r[3]=="a"?r.slice(5):r.slice(4)),x.g=i(g),x.b=i(b),x.a=a?parseFloat(a):-1
		}else{
			if(n==8||n==6||n<4)return null;
			if(n<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(n>4?d[4]+d[4]:"");
			d=i(d.slice(1),16);
			if(n==9||n==5)x.r=d>>24&255,x.g=d>>16&255,x.b=d>>8&255,x.a=m((d&255)/0.255)/1000;
			else x.r=d>>16,x.g=d>>8&255,x.b=d&255,x.a=-1
		}return x};
	h=c0.length>9,h=a?c1.length>9?true:c1=="c"?!h:false:h,f=pSBCr(c0),P=p<0,t=c1&&c1!="c"?pSBCr(c1):P?{r:0,g:0,b:0,a:-1}:{r:255,g:255,b:255,a:-1},p=P?p*-1:p,P=1-p;
	if(!f||!t)return null;
	if(l)r=m(P*f.r+p*t.r),g=m(P*f.g+p*t.g),b=m(P*f.b+p*t.b);
	else r=m((P*f.r**2+p*t.r**2)**0.5),g=m((P*f.g**2+p*t.g**2)**0.5),b=m((P*f.b**2+p*t.b**2)**0.5);
	a=f.a,t=t.a,f=a>=0||t>=0,a=f?a<0?t:t<0?a:a*P+t*p:0;
	if(h)return"rgb"+(f?"a(":"(")+r+","+g+","+b+(f?","+m(a*1000)/1000:"")+")";
	else return"#"+(4294967296+r*16777216+g*65536+b*256+(f?m(a*255):0)).toString(16).slice(1,f?undefined:-2)

};