// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

const theTable = document.getElementById('filesTable');
const theTableBody = theTable.getElementsByTagName('tbody')[0];
const searchField = document.getElementById('searchField');

let treeview;

// Функция открытия папки
const tableOpenDir = function(data) {
  let id;
  if (typeof data == 'string') {
    // id = row.dataset.id;
    id = data;
  } else {
    // event.preventDefault();
    id = this.closest('tr').dataset.id;
  }
  const urlParams = new URLSearchParams(window.location.search);
  urlParams.set('directory', id);
  history.pushState({ directory: id }, '', window.location.origin + window.location.pathname + '?' + urlParams.toString());
  tableUpdate();
};

// Обработка нажатия кнопки "редактировать"
const tableEdit = function() {
  const row = this.closest('tr');
  // Если это документ, просто открываем его
  if (row.dataset.type === 'document') {
    // (this.closest('tr').dataset.id);
  } else {
    // Если это папка, вызываем диалог переименования
    const modalform = document.getElementById('modalRename');
    modalform.dataset.id = row.dataset.id;
    document.getElementById('modalRenameName').value = '';
    modalform.classList.add('is-active');
  }
};

// Переименование папки
const tableEditAccept = function() {
  const modalForm = document.getElementById('modalRename');
  const params = {};
  params.id = modalForm.dataset.id;
  params.name = document.getElementById('modalRenameName').value;

  fetchPost('/tools/list/directory.json', params)
    .then(res => {
      modalForm.classList.remove('is-active');
      return handleCodes(res);
    })
    .then(() => tableUpdate())
    .catch(e => {
      // Если это уже ошибка, нет смысла создавать новую ошибку
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Отправляет нас на уровень вверх
const tableReturn = function() {
  event.preventDefault();
  const urlParams = new URLSearchParams(window.location.search);
  // Только если мы не в корне
  if ((!urlParams.has('directory')) || (urlParams.get('directory') === '/')) { return; }
  // Получаем инфу о папке
  fetchGet('/tools/list/directory.json', { id: urlParams.get('directory') })
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      tableOpenDir(data.parent);
    })
    .catch(e => {
      // Если это уже ошибка, нет смысла создавать новую ошибку
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Копирует документ и открывает его
const tableCopy = function() {
  const row = this.closest('tr');
  // Получаем копируемый документ
  fetchGet('/tools/list/document.json', { id: row.dataset.id })
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      const params = {};
      // Новое имя
      params.name = 'Копия ' + data.name;
      // Отсеиваем все запрещённые поля, ставим правильные
      const disallowedFields = ['id', '_id', 'name', 'created', 'updated', 'type'];
      for (const [key, value] of Object.entries(data)) {
        if (!disallowedFields.includes(key)) {
          params[key] = value;
        }
      }
      return fetchPost('/tools/list/document.json', params);
    })
    .then(res2 => handleCodes(res2))
    .then(res2 => res2.json())
    .then(data2 => {
      if (data2.insertedId) {
        window.open('/tools/?id=' + data2.insertedId, '_blank').focus();
        tableUpdate();
      } else {
        throw new Error('Сервер не вернул ID новой копии документа.');
      }
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Перемещение документа
const tableMoveOpen = function() {
  // Рекурсивная функция создания дерева
  const setChildren = function(node, source) {
    source.children.forEach(el => {
      if (row.dataset.type === 'directory' && row.dataset.id === el._id) {
        // Нельзя перемещать папку в саму себя и её детей
        return;
      }
      const newchild = new TreeNode(el.name);
      newchild.changeOption('id', el._id);
      node.addChild(newchild);
      setChildren(newchild, el);
    });
  };

  // Сохраняем тип и айди перемещаемого документа
  const row = this.closest('tr');
  const modalForm = document.getElementById('modalSave');
  modalForm.dataset.id = row.dataset.id;
  modalForm.dataset.type = row.dataset.type;

  // Делаем запрос
  fetchGet('/tools/list/directorylist.json', {})
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      const root = new TreeNode('root', { id: '/' });
      if (treeview) {
        // Очистка всех нод
        treeview.setRoot(root);
      } else {
        // Создание нового дерева
        treeview = new TreeView(root, document.getElementById('treeview'));
        treeview.changeOption('leaf_icon', '<i class="fas fa-folder"></i>');
        treeview.changeOption('parent_icon', '<i class="fas fa-folder"></i>');
      }
      // Формируем дерево
      setChildren(root, data);
      root.setSelected(true);
      treeview.reload();
      document.getElementById('modalSave').classList.add('is-active');
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Перемещает папку или файл в указанное место
const tableMoveAccept = function() {
  const modalForm = document.getElementById('modalSave');
  // let newParent = treeview.getSelectedNodes()[0].getOptions().id;
  const urlAPI = modalForm.dataset.type === 'directory' ? '/tools/list/directory.json' : '/tools/list/document.json';
  fetchPost(urlAPI, {
    id: modalForm.dataset.id,
    move: treeview.getSelectedNodes()[0].getOptions().id,
  })
    .then(res => {
      modalForm.classList.remove('is-active');
      return handleCodes(res);
    })
    .then(() => tableUpdate())
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Открывает субдиалог создания новой папки
const newDirOpen = function() {
  document.getElementById('modalNewDirName').value = '';
  document.getElementById('modalNewDir').classList.add('is-active');
};

// Создаёт новую папку
const newDirAccept = function() {
  const aParent = treeview.getSelectedNodes()[0];
  const name = document.getElementById('modalNewDirName').value;
  // aParentID = aParent.getOptions().id;
  fetchPost('/tools/list/directory.json', {
    name,
    parent: aParent.getOptions().id,
  })
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      const newChild = new TreeNode(name, { id: data.insertedId });
      aParent.addChild(newChild);
      aParent.setSelected(false);
      newChild.setSelected(true);
      treeview.reload();
      document.getElementById('modalNewDir').classList.remove('is-active');
    })
    .catch(e => {
      document.getElementById('modalNewDir').classList.remove('is-active');
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Открывает диалог удаления файла или документа
const tableDelete = function() {
  const row = this.closest('tr');
  const modalForm = document.getElementById('modalDelete');
  modalForm.dataset.id = row.dataset.id;
  modalForm.dataset.type = row.dataset.type;
  document.getElementById('modalDeleteType').innerText = row.dataset.type === 'directory' ? 'папку' : 'документ';
  document.getElementById('modalDeleteName').innerText = row.getElementsByClassName('fileName')[0].innerText;
  modalForm.classList.add('is-active');
};

// Производит непосредственно удаление
const tableDeleteAccept = function() {
  const modalForm = document.getElementById('modalDelete');
  const params = { id: modalForm.dataset.id };
  const apilink = modalForm.dataset.type === 'directory' ? '/tools/list/directory.json' : '/tools/list/document.json';

  fetchDelete(apilink, params)
    .then(res => handleCodes(res))
    .then(() => {
      modalForm.classList.remove('is-active');
      tableUpdate();
    })
    .catch(e => {
      modalForm.classList.remove('is-active');
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Удаление всех строк
const tableClear = function() {
  const theTableChildren = theTable.querySelectorAll('tbody tr:not(.hide)');

  for (const el of theTableChildren) {
    el.remove();
  }
};

// Добавляет папку или документ в таблицу
const tableAdd = function(obj) {
  // Ищем образец и клонируем его
  const temp = theTableBody.getElementsByClassName('hide')[0].cloneNode(true);
  // Делаем видимым
  temp.classList.remove('hide');
  // Навешиваем обработчики кнопок
  temp.getElementsByClassName('fileEdit')[0].addEventListener('click', tableEdit, false);
  if (obj.type === 'directory') {
    temp.getElementsByClassName('fileCopy')[0].remove();
    temp.getElementsByClassName('fileName')[0].addEventListener('click', tableOpenDir, false);
  } else {
    temp.getElementsByClassName('fileCopy')[0].addEventListener('click', tableCopy, false);
    temp.getElementsByClassName('fileName')[0].href = '/tools?id=' + obj.id;
  }
  temp.getElementsByClassName('fileMove')[0].addEventListener('click', tableMoveOpen, false);
  temp.getElementsByClassName('fileDelete')[0].addEventListener('click', tableDelete, false);
  // Заполняем свойства
  temp.dataset.id = obj.id;
  temp.dataset.type = obj.type;
  if (obj.type === 'directory') {
    temp.getElementsByClassName('fileIcon')[0].innerHTML = '<span><i class="far fa-folder"></i></span>';
    temp.getElementsByClassName('fileHeader')[0].innerText = '[Папка]';
  } else {
    temp.getElementsByClassName('fileHeader')[0].innerText = obj.header;
  }
  temp.getElementsByClassName('fileName')[0].innerText = obj.name;
  temp.getElementsByClassName('fileCreated')[0].innerText = obj.created;
  temp.getElementsByClassName('fileUpdated')[0].innerText = obj.updated;
  // Суём в конец tbody
  theTableBody.appendChild(temp);
};

// Добавляет строку, возвращающую на уровень выше
const tableAddReturn = function() {
  // Ищем образец и клонируем его
  const temp = theTableBody.getElementsByClassName('hide')[0].cloneNode(true);
  // Делаем видимым
  temp.classList.remove('hide');
  // Удаляем кнопки
  temp.getElementsByClassName('fileEdit')[0].remove();
  temp.getElementsByClassName('fileCopy')[0].remove();
  temp.getElementsByClassName('fileMove')[0].remove();
  temp.getElementsByClassName('fileDelete')[0].remove();
  temp.getElementsByClassName('fileName')[0].addEventListener('click', tableReturn, false);
  // Заполняем свойства
  const urlParams = new URLSearchParams(window.location.search);
  temp.dataset.id = urlParams.get('directory');
  temp.dataset.type = 'directory';
  temp.getElementsByClassName('fileIcon')[0].innerHTML = '<span><i class="far fa-folder"></i></span>';
  temp.getElementsByClassName('fileName')[0].innerText = '[Вернуться]';
  temp.getElementsByClassName('fileHeader')[0].innerText = '[Вернуться]';
  temp.getElementsByClassName('fileCreated')[0].innerText = '';
  temp.getElementsByClassName('fileUpdated')[0].innerText = '';
  // Суём в конец tbody
  theTableBody.appendChild(temp);
};

// Получить записи из БД
const tableUpdate = function() {
  const params = {};
  // Обработка строки поиска
  if (searchField.value) {
    params.search = searchField.value;
  }
  // Обработка папок (текущего пути)
  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has('directory')) {
    params.directory = urlParams.get('directory');
  } else {
    params.directory = '/';
  }
  // TODO: Здесь должен будет быть pagination
  fetchGet('/tools/list/all.json', params)
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      tableClear();
      if (params.directory !== '/') {
        tableAddReturn();
      }
      data.forEach(obj => tableAdd(obj));
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Создание новой папки
const tableNewDirectory = function() {
  const params = { name: document.getElementById('modalCreateDirectoryName').value };
  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has('directory')) {
    params.parent = urlParams.get('directory');
  } else {
    params.parent = '/';
  }
  fetchPost('/tools/list/directory.json', params)
    .then(res => handleCodes(res))
    .then(() => {
      document.getElementById('modalCreateDirectory').classList.remove('is-active');
      tableUpdate();
    })
    .catch(e => {
      document.getElementById('modalCreateDirectory').classList.remove('is-active');
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Создание нового документа
const tableNewDocument = function() {
  const params = { name: document.getElementById('modalCreateDocumentName').value };
  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has('directory')) {
    params.parent = urlParams.get('directory');
  } else {
    params.parent = '/';
  }
  fetchPost('/tools/list/document.json', params)
    .then(res => handleCodes(res))
    .then(() => {
      document.getElementById('modalCreateDocument').classList.remove('is-active');
      tableUpdate();
    })
    .catch(e => {
      document.getElementById('modalCreateDocument').classList.remove('is-active');
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('searchButton').addEventListener('click', tableUpdate, false);
  document.getElementById('refreshButton').addEventListener('click', tableUpdate, false);
  document.getElementById('createDirectoryAccept').addEventListener('click', tableNewDirectory, false);
  document.getElementById('createDocumentAccept').addEventListener('click', tableNewDocument, false);
  document.getElementById('deleteAccept').addEventListener('click', tableDeleteAccept, false);
  document.getElementById('renameAccept').addEventListener('click', tableEditAccept, false);
  document.getElementById('modalSaveAccept').addEventListener('click', tableMoveAccept, false);
  document.getElementById('modalSaveNewDir').addEventListener('click', newDirOpen, false);
  document.getElementById('newDirAccept').addEventListener('click', newDirAccept, false);

  // Открывает диалог создания папки
  document.getElementById('createDirectoryButton').addEventListener('click', function() {
    document.getElementById('modalCreateDirectoryName').value = '';
    document.getElementById('modalCreateDirectory').classList.add('is-active');
  }, false);

  // Открывает диалог создания документа
  document.getElementById('createDocumentButton').addEventListener('click', function() {
    document.getElementById('modalCreateDocumentName').value = '';
    document.getElementById('modalCreateDocument').classList.add('is-active');
  }, false);

  // Кнопка закрытия формы
  const temp = document.getElementsByClassName('modalClose');
  for (const el of temp) {
    el.addEventListener('click', function() {
      this.closest('.modal').classList.remove('is-active');
    }, false);
  }

  tableUpdate();
});
