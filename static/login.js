// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

const message = document.getElementById('message');
const pass = document.getElementById('pass');
const log = document.getElementById('log');

function setCookie(name, value, options = {}) {

  options = {
    path: '/',
    // при необходимости добавлю другие значения по умолчанию
    ...options
  };

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}

const loginVerify = async function(e = false) {
	if(e) {
		e.preventDefault();
	} 
	
	let xhr = new XMLHttpRequest();
	xhr.open("POST", '/tools/login', true);

	//Передаёт правильный заголовок в запросе
	xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
	xhr.responseType = 'json';

	//Вызывает функцию при смене состояния.
	xhr.onreadystatechange = () => {
		if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
			// Запрос завершён. Здесь можно обрабатывать результат.
			message.innerHTML = xhr.response.code;
			if ((xhr.response.code == 'OK') && (xhr.response.token)) {
				setCookie('token', xhr.response.token, {'max-age': 43200, 'sameSite':'Lax'});
				// Запомним логин на будущее
				window.localStorage.setItem('login', log.value.trim());
				// Если у нас есть параметр редиректа, отправляем на него
				const urlParams = new URLSearchParams(window.location.search);
				if (urlParams.get('redirect')) {
					window.location.href = decodeURIComponent(urlParams.get('redirect'));
				} else {
					// Иначе перенаправим на дефолтную страницу
					window.location.href = '/tools';
				}
			}
		} else {
			message.innerHTML = 'Код ошибки: ' + xhr.status;
		}
	}
	xhr.send(JSON.stringify({
		'pass': pass.value.trim(),
		'log': log.value.trim()
		}));
}

const ready = function() {
	document.getElementById('commit').addEventListener('click', loginVerify, false);
	// pass.addEventListener('keyup', (e) => {
		// if (e.keyCode === 13) {
			// loginVerify();
		// }
	// });
	// pass.addEventListener('submit', (e) => {
		// e.preventDefault()
	// })
}

document.addEventListener("DOMContentLoaded", ready);

document.addEventListener('submit', loginVerify, false)