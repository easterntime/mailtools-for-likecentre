// ================================
// Copyright 2021 © Волегов Александр
// Author Волегов Александр
// Licensed under the Apache License, Version 2.0
// ================================

// Словарик
let types;

// Добавление карточек шаблонов тегов
const cardsAdd = function(sectionID, fillObj = false) {
  let theSection;
  if (sectionID.nodeType) {
    // По элементу
    theSection = sectionID;
  } else {
    // По id
    theSection = document.getElementById(sectionID);
  }
  // Ищем образец и клонируем его
  const temp = theSection.getElementsByClassName('is-hidden')[0].cloneNode(true);
  // Делаем видимым
  temp.classList.remove('is-hidden');
  // Суём в конец секции
  theSection.appendChild(temp);

  // Если строку надо предзаполнить, заполняем
  if (!fillObj) { return; }
  // {id: '', tag: ['tag'], start: '', end: '', argAlt: '', type: 'single'}

  const isPaired = fillObj.type === 'paired' || fillObj.end;

  temp.id = fillObj.id;
  temp.getElementsByClassName('data-tag-opening')[0].value = fillObj.start;
  if (isPaired) {
    temp.getElementsByClassName('data-tag-title-opening')[0].innerText = '[' + (Array.isArray(fillObj.tag) ? fillObj.tag.join('], [') : fillObj.tag) + '] открывающий';
    temp.getElementsByClassName('data-tag-closing')[0].value = fillObj.end;
    temp.getElementsByClassName('data-tag-title-closing')[0].innerText = '[/' + (Array.isArray(fillObj.tag) ? fillObj.tag.join('], [/') : fillObj.tag) + '] закрывающий';
  } else {
    temp.getElementsByClassName('data-tag-title-opening')[0].innerText = (Array.isArray(fillObj.tag) ? fillObj.tag.join(', ') : fillObj.tag);
  }
  temp.getElementsByClassName('data-argument-alt')[0].value = fillObj.argAlt;
};

// Удаление всех карточек шаблонов тегов
const cardsClear = function(sectionID) {
  let sectionChildren;
  if (sectionID.nodeType) {
    // По элементу
    sectionChildren = sectionID.querySelectorAll('.tag-card:not(.is-hidden)');
  } else {
    // По id
    sectionChildren = document.getElementById(sectionID).querySelectorAll('.tag-card:not(.is-hidden)');
  }

  for (const el of sectionChildren) {
    el.remove();
  }
};

// Формирует массив из данных карточек, секция с которыми передана как аргумент
const cardsExport = function(sectionID) {
  let sectionChildren;
  if (sectionID.nodeType) {
    // По элементу
    sectionChildren = sectionID.querySelectorAll('.tag-card:not(.is-hidden)');
  } else {
    // По id
    sectionChildren = document.getElementById(sectionID).querySelectorAll('.tag-card:not(.is-hidden)');
  }
  const data = [];

  for (const el of sectionChildren) {
    const temp = {};
    temp.id = el.id;
    temp.start = el.getElementsByClassName('data-tag-opening')[0].value;
    // У одиночных тегов нет закрывающего тега. Фильтруем их.
    const closingEl = el.getElementsByClassName('data-tag-closing');
    if (closingEl.length > 0) {
      temp.end = closingEl[0].value;
    }
    temp.argAlt = el.getElementsByClassName('data-argument-alt')[0].value;
    data.push(temp);
  }

  return data;
  // {tag: {opening:"", closing:"", argumentAlt:""}}
};

// Подгрузка цветов с сервера
const colorsUpdate = function() {
  const theTable = document.getElementById('tableColors');
  // Запрос
  fetchGet('/tools/settings/colors.json', {})
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      // Обновление таблицы
      tableClear(theTable);
      data.forEach(obj => tableAddByCode(theTable, obj, { id: obj.id }));
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Отправляет цвета на сервер
const colorsSave = function() {
  // Элемент - кнопка сохранения
  const elButton = document.getElementById('colorsSave');
  const oldText = elButton.innerHTML;
  // Добавляем анимацию загрузки
  elButton.classList.add('is-loading');

  // Отправляем изменения на сервер
  fetchPost('/tools/settings/colors.json', tableExport('tableColors'))
    .then(res => handleCodes(res))
    .then(res => {
      // Апдейтим таблицы
      colorsUpdate();
      // Убираем анимацию загрузки
      elButton.classList.remove('is-loading');
      // Добавляем галочку
      elButton.innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Сохранено!</span>';
      // Возвращаем кнопку в нормальное состояние через 2 секунды
      setTimeout(() => {
        elButton.innerHTML = oldText;
      }, 2000);
    })
    .catch(e => {
      // Добавляем крестик
      elButton.innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка!</span>';
      // Возвращаем кнопку в нормальное состояние через 2 секунды
      setTimeout(() => {
        elButton.innerHTML = oldText;
      }, 2000);
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Подгрузка тегов с сервера
const tagsUpdate = function(type) {
  const theTable = document.getElementById(types[type].table);
  const theCard = document.getElementById(types[type].cards);

  // Запрос
  fetchGet(types[type].api, {})
    .then(res => handleCodes(res))
    .then(res => res.json())
    .then(data => {
      // Обновление таблицы
      tableClear(theTable);
      // Обновление карточек
      cardsClear(theCard);
      data.forEach(obj => {
        tableAddByCode(theTable, obj, { id: obj.id, protected: obj.protected });
        cardsAdd(theCard, obj);
      });
    })
    .catch(e => {
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Для парных тегов
const tagsPairedUpdate = function() {
  tagsUpdate('paired');
};

// Для непарных тегов
const tagsSingleUpdate = function() {
  tagsUpdate('single');
};

// Отправляет теги из таблицы на сервер
const tagsTableSave = function(type) {
  // Элемент - кнопка сохранения
  const elButton = document.getElementById(types[type].tableButton);
  const oldText = elButton.innerHTML;
  // Добавляем анимацию загрузки
  elButton.classList.add('is-loading');

  // Отправляем изменения на сервер
  fetchPost(types[type].api, tableExport(types[type].table))
    .then(res => handleCodes(res))
    .then(res => {
      // Апдейтим таблицы
      types[type].update();
      // Убираем анимацию загрузки
      elButton.classList.remove('is-loading');
      // Добавляем галочку
      elButton.innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Сохранено!</span>';
      // Возвращаем кнопку в нормальное состояние через 2 секунды
      setTimeout(() => {
        elButton.innerHTML = oldText;
      }, 2000);
    })
    .catch(e => {
      // Добавляем крестик
      elButton.innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка!</span>';
      // Возвращаем кнопку в нормальное состояние через 2 секунды
      setTimeout(() => {
        elButton.innerHTML = oldText;
      }, 2000);
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Для парных тегов
const tagsPairedTableSave = function() {
  tagsTableSave('paired');
};

// Для непарных тегов
const tagsSingleTableSave = function() {
  tagsTableSave('single');
};

// Отправляет шаблоны из карточек на сервер
const tagsCardsSave = function(type) {
  // Элемент - кнопка сохранения
  const elButton = document.getElementsByClassName(types[type].cardsButton);
  const oldText = elButton[0].innerHTML;
  // Добавляем анимацию загрузки
  elButton[0].classList.add('is-loading');
  elButton[1].classList.add('is-loading');

  // Отправляем изменения на сервер
  fetchUpdate(types[type].api, cardsExport(types[type].cards))
    .then(res => handleCodes(res))
    .then(res => {
      // Апдейтим таблицы
      types[type].update();
      // Убираем анимацию загрузки
      elButton[0].classList.remove('is-loading');
      elButton[1].classList.remove('is-loading');
      // Добавляем галочку
      elButton[0].innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Сохранено!</span>';
      elButton[1].innerHTML = '<span class="icon"><strong>&check;</strong></span><span>Сохранено!</span>';
      // Возвращаем кнопку в нормальное состояние через 2 секунды
      setTimeout(() => {
        elButton[0].innerHTML = oldText;
        elButton[1].innerHTML = oldText;
      }, 2000);
    })
    .catch(e => {
      // Добавляем крестик
      elButton[0].innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка!</span>';
      elButton[1].innerHTML = '<span class="icon"><strong>&#10060;</strong></span><span>Ошибка!</span>';
      // Возвращаем кнопку в нормальное состояние через 2 секунды
      setTimeout(() => {
        elButton[0].innerHTML = oldText;
        elButton[1].innerHTML = oldText;
      }, 2000);
      if (e instanceof Error) {
        handleCatch(e);
      } else {
        handleCatch(new Error(JSON.stringify(e)));
      }
    });
};

// Для парных тегов
const tagsPairedCardsSave = function() {
  tagsCardsSave('paired');
};

// Для непарных тегов
const tagsSingleCardsSave = function() {
  tagsCardsSave('single');
};

// Переключение вкладок
const tabButtons = document.querySelectorAll('.tabs li');
const tabContent = document.getElementsByClassName('tab-pane');

const toggleTab = function() {
  if (!this.dataset.target) { return; };

  for (const el of tabButtons) {
    if (el.dataset.target === this.dataset.target) {
      el.classList.add('is-active');
    } else {
      el.classList.remove('is-active');
    }
  }

  for (const el of tabContent) {
    if (el.id === this.dataset.target) {
      el.classList.add('is-active');
    } else {
      el.classList.remove('is-active');
    }
  }
};

document.addEventListener('DOMContentLoaded', () => {
  // Вкладки
  for (const el of tabButtons) {
    el.addEventListener('click', toggleTab, false);
  }

  // Словарик
  types = {
    paired: {
      api: '/tools/settings/tagsPaired.json',
      table: 'tableTagsPaired',
      cards: 'cardsTagsPaired',
      tableButton: 'tagsPairedTableSave',
      cardsButton: 'tagsPairedCardsSave',
      update: tagsPairedUpdate,
    },
    single: {
      api: '/tools/settings/tagsSingle.json',
      table: 'tableTagsSingle',
      cards: 'cardsTagsSingle',
      tableButton: 'tagsSingleTableSave',
      cardsButton: 'tagsSingleCardsSave',
      update: tagsSingleUpdate,
    },
  };

  // Кнопки
  document.getElementById('colorsSave').addEventListener('click', colorsSave);
  document.getElementById('colorsUpdate').addEventListener('click', colorsUpdate);
  document.getElementById(types.paired.tableButton).addEventListener('click', tagsPairedTableSave);
  document.getElementById(types.single.tableButton).addEventListener('click', tagsSingleTableSave);
  let temp = document.getElementsByClassName('tagsPairedUpdate');
  for (const el of temp) {
    el.addEventListener('click', tagsPairedUpdate, false);
  }
  temp = document.getElementsByClassName('tagsSingleUpdate');
  for (const el of temp) {
    el.addEventListener('click', tagsSingleUpdate, false);
  }
  // Кнопок сохранения карточек по две - сверху и снизу
  temp = document.getElementsByClassName(types.paired.cardsButton);
  for (const el of temp) {
    el.addEventListener('click', tagsPairedCardsSave, false);
  }
  temp = document.getElementsByClassName(types.single.cardsButton);
  for (const el of temp) {
    el.addEventListener('click', tagsSingleCardsSave, false);
  }
  // Загрузка при открытии
  colorsUpdate();
  tagsPairedUpdate();
  tagsSingleUpdate();
});
